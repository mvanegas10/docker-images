# Docker ECR login
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 435994096874.dkr.ecr.eu-west-1.amazonaws.com

# Create image from Dockerfile
docker build -t 435994096874.dkr.ecr.eu-west-1.amazonaws.com/assigned-cols-actito .

# Open interactive bash console
docker run -it 435994096874.dkr.ecr.eu-west-1.amazonaws.com/assigned-cols-actito

# Push image to ECR
docker commit <id container> <tag>
docker push <tag>

# Remove docker images and containers
docker rm $(docker ps -a | awk 'NR>1 {print $1}')
docker image rm $(docker images | grep -v "ubuntu" | grep -v "nginx" | awk 'NR>1 {print $3}')

# Port mapping
docker run -d -p :8080:80 <tag>