#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
import requests
import boto3
import json
import os
import io
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import configparser
import multiprocessing
from multiprocessing import Pool
import hashlib
num_partitions = 5
num_cores = multiprocessing.cpu_count()


# ## Utilitaires
def request_video(row):
    try:
        r = requests.get('https://www.rtbf.be/api/partner/generic/article/articledetail?partner_key=4ce1dd2e90acd38cb5cffb24ee1621bb&v=8&target_site=rtbfinfo&article_id={}'.format(row['id']))
        medias = []
        audios = []
        if r.status_code == 200:
            response = json.loads(r.text)
            if 'videos' in response:
                medias.extend([format_media(row['id'], m) for m in response['videos']])
            if 'audios' in response:
                medias.extend([format_media(row['id'], m) for m in response['audios']])  
            if 'embeds' in response:
                for embed in response['embeds']:
                    result = process_embed(row['id'], embed)
                    if result is not None:
                        if result['type'] == 'video':
                            medias.append(format_media(row['id'], result))
                        else:
                            medias.append(format_media(row['id'], result))                                       
            if 'paragraphs' in response:
                for par in response['paragraphs']:
                    if 'videos' in par:
                        medias.extend([format_media(row['id'], m) for m in par['videos']])
                    elif 'audios' in par:
                        medias.extend([format_media(row['id'], m) for m in par['audios']])                            
                    elif 'embeds' in par:
                        for embed in par['embeds']:
                            result = process_embed(row['id'], embed)
                            if result is not None:
                                if result['type'] == 'video':
                                    medias.append(result)
                                else:
                                    medias.append(result)   
            if 'paragraph' in response and type(response['paragraph']) == list:
                for par in response['paragraph']:
                    if 'video' in par:
                        medias.extend([format_media(row['id'], m) for m in par['video']])
                    if 'audio' in par:
                        medias.extend([format_media(row['id'], m) for m in par['audio']])                          
                    if 'embed' in par:
                        for embed in par['embed']:
                            result = process_embed(row['id'], embed)
                            if result is not None:
                                if result['type'] == 'video':
                                    medias.append(result)
                                else:
                                    medias.append(result)
        row['medias'] = medias if len(medias) > 0 else np.nan
    except Exception as e:
        print(e)
    return row


def process_embed(articleid, embed):
    if 'type' in embed and 'url' in embed:
        return {
            'articleid': int(articleid),
            'id': embed['id'] if 'id' in embed else '',
            'type': embed['type'],
            'url': embed['url'],
            'domain': str(urlparse(embed['url']).netloc),
            'duration': create_float(embed['duration']) if 'duration' in embed else np.nan
        }
    elif 'code' in embed:
        soup = BeautifulSoup(embed['code'], 'html.parser')
        iframes = soup.find_all('iframe')
        if len(iframes) > 0:
            src = iframes[0].get('src')
            if src is not None and 'video' in src:
                return {
                    'articleid': int(articleid),
                    'id': embed['id'] if 'id' in embed else '',
                    'type': 'video',
                    'url': src,
                    'domain': str(urlparse(src).netloc),
                    'duration': create_float(embed['duration']) if 'duration' in embed else np.nan
                }
            elif src is not None and ('audio' in src or 'podcast' in src): 
                return {
                    'articleid': int(articleid),
                    'id': embed['id'] if 'id' in embed else '',
                    'type': 'audio',
                    'url': src,
                    'domain': str(urlparse(src).netloc),
                    'duration': create_float(embed['duration']) if 'duration' in embed else np.nan
                }


def create_float(x):
    try:
        return float(x)
    except:
        return np.nan


def format_media(articleid, media):
    obj = {
        'articleid': int(articleid),
        'id': media['id'] if 'id' in media else '', 
        'type': media['type'] if 'type' in media else '', 
        'url': media['url'] if 'url' in media else '', 
        'domain': str(urlparse(media['url']).netloc),
        'duration': create_float(media['duration']) if 'duration' in media else np.nan
    }
    return obj                


def parallelize_dataframe(df, func):
    a,b,c,d,e = np.array_split(df, num_partitions)
    pool = Pool(num_cores)
    df = pd.concat(pool.map(func, [a,b,c,d,e]))
    pool.close()
    pool.join()
    return df


def parallel_func(data):
    data = data.apply(lambda row: request_video(row), axis=1)
    return data


if __name__=='__main__':
    try:

        # Configuration parser
        config = configparser.ConfigParser()
        config.read('config.ini')
        # config.read('/home/ubuntu/scripts/config.ini')

        # Internal AWS resources
        ssm = boto3.client('ssm')
        s3 = boto3.client('s3')
        s3_resource = boto3.resource('s3')  

        parameter = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        bucket = 'big-data-media'
        
        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(config['FULL_LOAD']['db_username'], parameter['Parameter']['Value'], config['FULL_LOAD']['db_host'], config['FULL_LOAD']['db_port'], config['FULL_LOAD']['db_database']),pool_recycle=600)

        start_date = datetime.datetime.strptime(config['FULL_LOAD']['start_date'], '%Y-%m-%d').replace(hour=0, minute=0, second=0, microsecond=0)
        today = datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)

        tmp_date = start_date
        while tmp_date < today:
            df_art = pd.read_sql_query("""SELECT id FROM article_article WHERE published = '1' AND feed IN ('RTBFINFO','RTBFSPORT','RTBFTENDANCE','RTBFCULTURE', 'TV', 'MUSIQ3', 'CLASSIC21', 'TIPIK', 'VIVACITE', 'LAPREMIERE')  AND DATE_TRUNC('day', displaydate) = '{}'""".format(tmp_date.strftime('%Y-%m-%d')), engine)
            df_art['id'] = df_art['id'].astype(int)
            if len(df_art) > 0:
                df_art['medias'] = np.nan
                print(df_art.count())
                df_art_proc = parallelize_dataframe(df_art, parallel_func)
                df_art_proc = df_art_proc[~(df_art_proc['medias'].isna())]
                print(df_art_proc.count())
                medias = df_art_proc['medias'].apply(pd.Series).stack().reset_index(drop=True)
                
                if len(medias) > 0:
                    df_medias = pd.DataFrame(list(medias))
                    df_medias['mediaid'] = df_medias.apply(lambda x: hashlib.sha1('{}'.format(x['articleid'], x.index).encode()).hexdigest(), axis=1)

                    try:
                        csv_buffer_internal = io.StringIO()
                        df_medias[['articleid','id','mediaid','type','url','domain','duration']].to_csv(csv_buffer_internal, index=False, encoding='utf-8-sig')
                        response = s3_resource.Object(bucket, 'in/cryo/articles_media/{}.csv'.format(tmp_date.strftime('%Y/%m/%d/%Y%m%dT%H:00:00.000'))).put(Body=csv_buffer_internal.getvalue())
                        print(tmp_date.strftime('%Y%m%dT%H:00:00.000'))
                    except Exception as e:
                        print(e)    
                        df_medias.to_csv('{}.csv'.format(datetime.datetime.strftime(tmp_date, '%y%m%d')), index=False, encoding='utf-8-sig')
            tmp_date = tmp_date + datetime.timedelta(1)

        print({
            'statusCode': 200,
            'body': json.dumps('{} : {} Finished full load /in/cryo/articles_media/'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y-%m-%d')))
        })

    except Exception as e:
        print({
            'statusCode': 500,
            'body': json.dumps('{} : Could not finish load {}'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y%m%d'), str(e)))
        })