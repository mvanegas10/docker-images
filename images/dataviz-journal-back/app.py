from flask import Flask, request, make_response, jsonify, send_from_directory
import sqlalchemy as sql
from flask_caching import Cache
from botocore.config import Config
import configparser
import requests
import boto3 
import json
import io
import base64


# CONFIG
cache = Cache(config={ "CACHE_TYPE": "SimpleCache", "CACHE_DEFAULT_TIMEOUT": 86400})
app = Flask(__name__, static_url_path='', static_folder='front')

config = configparser.ConfigParser()
config.read('config/default.ini')

aws_config = Config(region_name=config['AWS']['AWS_DEFAULT_REGION'])    

aws_session = boto3.session.Session(
    aws_access_key_id=config['AWS']['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key=config['AWS']['AWS_SECRET_ACCESS_KEY']            
)          

s3 = aws_session.client('s3', config=aws_config)
ssm = aws_session.client('ssm', config=aws_config)
redshift_secret = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)

engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(config['DATABASE']['username'], redshift_secret['Parameter']['Value'], config['DATABASE']['host'], config['DATABASE']['port'], config['DATABASE']['database']),pool_recycle=600) 

# CONSTANTS
CONNECTED = 'CONNECTED'


def verify_login(user):
    res = requests.get('https://cognito-idp.{}.amazonaws.com/{}/.well-known/jwks.json'.format(config['AWS']['AWS_DEFAULT_REGION'], config['AWS']['USER-POOL']))
    if res:
        jwks = json.loads(res.text)
        tokens = ['tokenId', 'accessToken']
        verifications = []

        for key in tokens:
            for token in user[key].split('.'):
                if len(token)%4 == 0:
                    decoded = base64.b64decode(token)
                    print(decoded)
                    obj = json.loads(decoded)
                    if 'kid' in obj:
                        if obj['kid'] in [x['kid'] for x in jwks['keys']]:
                            verifications.append(key)

        response = len(verifications) == len(tokens)

        if response:
            return CONNECTED
        else:
            raise Exception('User not connected')
    else:
        raise Exception('Service unavailable, please try later')


def decorate_response(response):
    response.headers.add("Access-Control-Allow-Origin", "*")
    response.headers.add('Access-Control-Allow-Headers', "*")
    response.headers.add('Access-Control-Allow-Methods', "*")
    return response


@app.route('/getUserSegments', methods=['POST'])
def getUserSegments():         
    try:

        emailsdata = s3.get_object(Bucket='big-data-media', Key='crm/in/segments/segments_baseline/email_mappings.json')
        emapping= json.load(io.BytesIO(emailsdata['Body'].read()))

        data = request.get_json()
        res = verify_login(data['user'])

        email = data['user']['email'].lower().strip()

        if res == CONNECTED:
            columns = ['type','name','title','segment','last_date','recency']
            with engine.connect() as conn:
                result = conn.execute("""
                    WITH
                    q0 AS (
                        (SELECT MD5(CONCAT(object_id,object_type)) AS hashmediatype, program_labelstat AS name, object_title AS title FROM egos_contents)
                            UNION ALL
                        (SELECT MD5(CONCAT(live_planning_id,'live')) AS hashmediatype, program_labelstat AS name, live_planning_title AS title FROM egos_live_contents)
                            UNION ALL
                        (SELECT MD5(CONCAT(id,'article')) AS hashmediatype, feed AS name, title FROM article_article)
                        )
                    , q1 AS (
                    SELECT MD5(CONCAT(media.mediaid,mediatype.mediatype)) AS hashmediatype, mediatype.mediatype AS type, q0.name, q0.title, MAX(dates.date) AS max_date
                        FROM datawarehouse.egos_fct_trackers tracker
                        INNER JOIN datawarehouse.egos_tracker_dim_date dates ON tracker.fk_date = dates.pk_date
                        INNER JOIN datawarehouse.egos_tracker_dim_users users ON tracker.fk_uid = users.pk_uid
                        INNER JOIN datawarehouse.egos_tracker_dim_media media ON tracker.fk_media = media.pk_media
                        INNER JOIN datawarehouse.egos_tracker_dim_mediatype mediatype ON tracker.fk_mediatype = mediatype.pk_mediatype
                        INNER JOIN egos_gigya gigya ON users.uid = gigya.uid
                        INNER JOIN q0 ON q0.hashmediatype = MD5(CONCAT(media.mediaid,mediatype.mediatype))
                    WHERE email = '{}'
                    GROUP BY hashmediatype, media.mediaid, type, name, title),
                    q2 AS (
                    SELECT MD5(CONCAT(id,media_type)) AS hashmediatype, segment FROM crm_segments_mapping),
                    q3 AS (SELECT type, name, title, segment, MAX(max) AS last_date, CASE WHEN last_date >= date_add('days',-7,CURRENT_DATE) THEN 'last_7_days'
                        WHEN last_date >= date_add('days',-30,CURRENT_DATE) THEN 'last_30_days'
                        WHEN last_date >= date_add('days',-90,CURRENT_DATE) THEN 'last_90_days'
                        WHEN last_date >= date_add('days',-180,CURRENT_DATE) THEN 'last_180_days'
                        WHEN last_date >= date_add('days',-365,CURRENT_DATE) THEN 'last_365_days'
                        ELSE 'older'
                        END AS recency
                    FROM
                    (SELECT *
                    FROM (
                        (SELECT type, name, title, segment, MAX(max_date) AS max
                        FROM q1 INNER JOIN q2 ON q1.hashmediatype = q2.hashmediatype
                        GROUP BY type, name, title, segment)
                        UNION ALL
                        (SELECT 'regsource' AS type, NULL AS name, regsource.regsource AS title, segment, created AS max
                        FROM egos_gigya gigya INNER JOIN egos_regsource_segment regsource ON gigya.regsource = regsource.regsource
                            WHERE email = '{}'
                        )
                    )
                    )
                    GROUP BY type, name, title, segment
                    )
                    SELECT q3.type, q3.name, q3.title, q3.segment, q3.last_date, q3.recency
                    FROM q3 INNER JOIN (
                        SELECT segment, MAX(last_date) AS max_last_date
                        FROM q3
                        GROUP BY segment
                        ) q4
                    ON q3.segment = q4.segment AND q3.last_date = q4.max_last_date
                    ORDER BY last_date DESC        
                """.format(emapping[email.lower().strip()] if email in emapping else email.lower().strip(), emapping[email.lower().strip()] if email in emapping else email.lower().strip()))
        
                resultlist = list(result)

            if len(resultlist) > 0:
                return decorate_response(jsonify({'data':[{col: row[i] if col != 'last_date' else row[i].strftime('%Y-%m-%d %H:%M:%S') for i, col in enumerate(columns)} for row in resultlist]}))
            else:
                return decorate_response(jsonify({'Error': 'No information was found for that given user.'}))

    except Exception as e:
        print('Exception: {}'.format(e))
        return decorate_response(jsonify({'Error': 'Bad request'}))


@app.route('/getFile', methods=['POST'])
def getFile():
    try:
        data = request.get_json()
        res = verify_login(data['user'])
        
        if res == CONNECTED:
            file_contents = s3.get_object(Bucket=data['bucket'], Key=data['prefix'])['Body'].read().decode('utf-8')            

            jsonfile = json.loads(file_contents)
            jsonfile['last_modified'] = s3.head_object(Bucket=data['bucket'], Key=data['prefix'])['LastModified'].strftime('%Y/%m/%d')

            return decorate_response(jsonify({'data': jsonfile }))

    except Exception as e:
        print('Exception: {}'.format(e))
        return decorate_response(jsonify({'Error': 'Bad request'}))


@app.route('/getValidSegments', methods=['POST'])
def getValidSegments():
    try:
        data = request.get_json()
        res = verify_login(data['user'])
        
        if res == CONNECTED:
            crm_team = json.loads(s3.get_object(Bucket='big-data-media', Key='crm/in/segments/segments_baseline/crm_team.json')['Body'].read().decode('utf-8'))

            if data['user']['email'] in crm_team:
                segments = {key:value['actito_col'] for key, value in json.load(io.BytesIO(s3.get_object(Bucket='big-data-media', Key='crm/in/segments/segments_baseline/segments_labels.json')['Body'].read())).items() if value['is_actito'] == 1}
                tables = json.loads(s3.get_object(Bucket='big-data-media', Key='crm/in/segments/segments_baseline/table_mappings.json')['Body'].read().decode('utf-8'))
                data = {
                    'segments': list(segments.values()),
                    'tables': list(tables.keys())
                }

                return decorate_response(jsonify({'data': data}))
            else:
                return decorate_response(jsonify({'Error': 'User not allowed to request method.'}))

    except Exception as e:
        print('Exception: {}'.format(e))
        return decorate_response(jsonify({'Error': 'Bad request'}))


@app.route('/getValidDimensions', methods=['POST'])
def getValidDimensions():
    try:
        data = request.get_json()
        res = verify_login(data['user'])
        
        if res == CONNECTED:
            crm_team = json.loads(s3.get_object(Bucket='big-data-media', Key='crm/in/segments/segments_baseline/crm_team.json')['Body'].read().decode('utf-8'))

            if data['user']['email'] in crm_team:            
                with engine.connect() as conn:
                    result = conn.execute("""SELECT col_name FROM pg_get_cols('{}') cols(view_schema name, view_name name, col_name name, col_type varchar, col_num int)""".format(data['table']))
                    return decorate_response(jsonify({'data': [row['col_name'] for row in result] }))

    except Exception as e:
        print('Exception: {}'.format(e))
        return decorate_response(jsonify({'Error': 'Bad request'}))        


@app.route('/addSegmentMapping', methods=['POST'])
def addSegmentMapping():
    try:
        data = request.get_json()
        res = verify_login(data['user'])
        
        if res == CONNECTED:
            crm_team = json.loads(s3.get_object(Bucket='big-data-media', Key='crm/in/segments/segments_baseline/crm_team.json')['Body'].read().decode('utf-8'))
            if data['user']['email'] in crm_team:  
                if 'segment' in data and 'table' in data and 'dimension' in data and 'value' in data:
                    with engine.connect() as conn:
                        conn.execute("""INSERT INTO public.crm_segments_baseline (segment, egos_table, dimension, variable) VALUES ('{}', '{}', '{}', '{}')""".format(data['segment'], data['table'], data['dimension'], data['value']))
                        return decorate_response(jsonify({'data': 'Successfully added register in table public.crm_segments_baseline.'}))
                else:
                    return decorate_response(jsonify({'Error': 'Missing data.'}))
            else:
                return decorate_response(jsonify({'Error': 'User not allowed to request method.'}))                    

    except Exception as e:
        print('Exception: {}'.format(e))
        return decorate_response(jsonify({'Error': 'Bad request'}))        


@app.route('/', defaults={'path':''})
def serve(path):
    return send_from_directory(app.static_folder,'index.html')  