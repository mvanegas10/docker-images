backcall==0.2.0
boto3==1.18.45
botocore==1.21.45
certifi==2021.5.30
cffi==1.14.6
charset-normalizer==2.0.6
click==8.0.1
decorator==5.1.0
ecdsa==0.17.0
envs==1.3
Flask==2.0.1
Flask-Caching==1.10.1
Flask-Cors==3.0.10
greenlet==1.1.1
idna==3.2
importlib-metadata==4.8.1
itsdangerous==2.0.1
jedi==0.18.0
Jinja2==3.0.1
jmespath==0.10.0
MarkupSafe==2.0.1
matplotlib-inline==0.1.3
parso==0.8.2
pexpect==4.8.0
pickleshare==0.7.5
prompt-toolkit==3.0.20
psycopg2-binary==2.9.1
ptyprocess==0.7.0
pyasn1==0.4.8
pycparser==2.20
Pygments==2.10.0
python-dateutil==2.8.2
requests==2.26.0
s3transfer==0.5.0
six==1.16.0
SQLAlchemy==1.4.23
traitlets==5.1.0
typing-extensions==3.10.0.2
urllib3==1.26.6
wcwidth==0.2.5
Werkzeug==2.0.1
zipp==3.5.0
