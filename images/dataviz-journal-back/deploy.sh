if  [[ $1 = "-d" ]]; then
    echo 'DEV'
    echo 'REACT_APP_API=/' > ~/Projects/react-data-viz/.env    
    cat ~/Projects/react-data-viz/package-test.json > ~/Projects/react-data-viz/package.json    
elif [[ $1 = "-p" ]]; then
    echo 'PROD'
    echo 'REACT_APP_API=https://viz.bda.rtbf.be/' > ~/Projects/react-data-viz/.env
    cat ~/Projects/react-data-viz/package-base.json > ~/Projects/react-data-viz/package.json    
fi

npm run build --prefix ~/Projects/react-data-viz/

cp -r ~/Projects/react-data-viz/build/*  front/

if [[ $1 = "-p" ]]; then
    aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 435994096874.dkr.ecr.eu-west-1.amazonaws.com

    docker build -t 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal-backend:latest .
    docker tag 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal-backend:latest 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal-backend:latest
    docker push 435994096874.dkr.ecr.eu-west-1.amazonaws.com/dataviz-journal-backend:latest

    kubectl delete -f deploy-conf.yml
    kubectl apply -f deploy-conf.yml
else
    source /home/meili/docker-images/images/dataviz-journal-back/env/bin/activate
    flask run
fi