import pandas as pd
import numpy as np
import unidecode
import datetime
import re
import sqlalchemy as sql
import os
import json
import time
import boto3
import io
import gzip
import csv
import sys


if __name__=='__main__':

    try:
        # ## Variables et constants

        ssm = boto3.client('ssm')
        s3 = boto3.client('s3')
        s3_resource = boto3.resource('s3')
        print('boto')
        redshift = boto3.client('redshift-data')
        print('redshift')

        bucket = s3_resource.Bucket('big-data-media')        

        parameter = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)

        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ['db_username'], parameter['Parameter']['Value'], os.environ['db_host'], os.environ['db_port'], os.environ['db_database']))
        today = datetime.datetime.now()
        
        filename = 'big-data-media/crm/out/actito/segments/segments'

        colnamesdata = s3.get_object(Bucket=bucket.name, Key='crm/in/segments/segments_baseline/segments_labels.json')
        column_names = {key:value['actito_col'] for key, value in json.load(io.BytesIO(colnamesdata['Body'].read())).items() if value['is_actito'] == 1}
       
        segments_cols = list(column_names.values())
        print(segments_cols)

        query1 = """WITH q1 AS ( SELECT actitoid, segment, recency, MAX(last_date) FROM egos_user_segments GROUP BY actitoid, segment, recency), q2 AS (SELECT actitoid AS actito_id"""

        query2 = ', q3 AS (SELECT actito_id'

        for seg in segments_cols:
            query1 += """, CASE WHEN segment = ''{}'' THEN q1.recency END AS {}""".format(seg, seg.lower())
            query2 += """, LISTAGG({},'''') WITHIN GROUP (ORDER BY {}) OVER (PARTITION BY actito_id) AS {}""".format(seg.lower(), seg.lower(), seg.lower())

        query1 += """ FROM q1)""".format('", "'.join(segments_cols))
        query2 += ' FROM q2)'

        query = """UNLOAD ('{} {} SELECT * FROM q3 GROUP BY actito_id, {} ') TO 's3://{}' iam_role 'arn:aws:iam::435994096874:role/myRedshiftRole' PARALLEL OFF HEADER CSV ALLOWOVERWRITE GZIP""".format(query1, query2, ', '.join([x.lower() for x in segments_cols]), filename)
        
        print(query)

        response = redshift.execute_statement(
            Database='a360',
            ClusterIdentifier='cluster-mktstr',
            DbUser=os.environ['db_username'],
            Sql=query
        )

        res = ''

        while res != 'FINISHED':
            response = redshift.describe_statement(
                Id=response['Id']
            )
            res = response['Status']
            time.sleep(5)

 
        for obj in bucket.objects.filter(Prefix='crm/out/actito/segments/'):
            if 'segments' in obj.key:
                data = s3_resource.Object(bucket.name, obj.key)
                with gzip.GzipFile(fileobj=data.get()['Body']) as gzfile:
                    df = pd.read_csv(io.BytesIO(gzfile.read()), encoding='utf-8-sig')

                    for col in column_names.keys():
                        if not col in df.columns:
                            df[col] = ''

                    df = df[~df['actito_id'].isna()]
                    df['actito_id'] = df['actito_id'].astype(int)

                    df = df.rename(columns=column_names)

                    csv_buffer_internal = io.StringIO()
                    df[['actito_id', *column_names.values()]].to_csv(csv_buffer_internal, index=False, encoding='utf-8-sig')
                    response = s3_resource.Object('big-data-media', 'playground/crm/test_send_actito.csv').put(Body=csv_buffer_internal.getvalue())
                    
                    df[['actito_id', *column_names.values()]].to_csv('/home/ubuntu/scripts/output/segments.csv', index=False, encoding='utf-8-sig')

        print({
            'statusCode': 200,
            'body': json.dumps('{} : Finished files generation.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))
        })

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        print(exc_type, fname, exc_tb.tb_lineno )        
        print({
            'statusCode': 500,
            'body': json.dumps('{} : Error generating files {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), e))
        })      