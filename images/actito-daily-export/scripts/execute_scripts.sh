/usr/local/bin/python /home/ubuntu/scripts/attribution_firstname.py
zip /home/ubuntu/scripts/output/attribution_firstname.zip /home/ubuntu/scripts/output/attribution_firstname.csv
authorization=$(curl -s -X GET "https://api.actito.com/auth/token"  -H "Authorization: 4646d291579a89ca7d014677f185d91d" -H "Accept: application/json" | jq -r '.accessToken')
curl -X POST "https://api.actito.com/v4/entity/-/table/People/import"  -H "Authorization: Bearer ${authorization}" -H "Accept: application/json"    -H "Content-Type: multipart/form-data"    -F "inputFile=@/home/ubuntu/scripts/output/attribution_firstname.zip;type=application/zip"    -F "parameters={\"mode\" : \"UPDATE_ONLY\", \"encoding\" : \"UTF-8\", \"format\" : { \"separator\" : \",\", \"endOfLine\" : \"\r\n\", \"enclosing\" : \"\\\"\", \"escaping\" : \"\\\"\" } };type=application/json"
rm /home/ubuntu/scripts/output/attribution_firstname.*
echo Sent predicted profile file

/usr/local/bin/python /home/ubuntu/scripts/attribution_postalcode.py
zip /home/ubuntu/scripts/output/attribution_codepostal.zip /home/ubuntu/scripts/output/attribution_codepostal.csv
authorization=$(curl -s -X GET "https://api.actito.com/auth/token"  -H "Authorization: 4646d291579a89ca7d014677f185d91d" -H "Accept: application/json" | jq -r '.accessToken')
curl -X POST "https://api.actito.com/v4/entity/-/table/People/import"  -H "Authorization: Bearer ${authorization}" -H "Accept: application/json"    -H "Content-Type: multipart/form-data"    -F "inputFile=@/home/ubuntu/scripts/output/attribution_codepostal.zip;type=application/zip"    -F "parameters={\"mode\" : \"UPDATE_ONLY\", \"encoding\" : \"UTF-8\", \"format\" : { \"separator\" : \",\", \"endOfLine\" : \"\r\n\", \"enclosing\" : \"\\\"\", \"escaping\" : \"\\\"\" } };type=application/json"
rm /home/ubuntu/scripts/output/attribution_codepostal.*
echo Sent geo file

/usr/local/bin/python /home/ubuntu/scripts/segments_generation.py
zip /home/ubuntu/scripts/output/segments.zip /home/ubuntu/scripts/output/segments.csv
authorization=$(curl -s -X GET "https://api.actito.com/auth/token"  -H "Authorization: 4646d291579a89ca7d014677f185d91d" -H "Accept: application/json" | jq -r '.accessToken')
curl -X POST "https://api.actito.com/v4/entity/RTBF/customTable/RecenceConsoSegments/import"  -H "Authorization: Bearer ${authorization}" -H "Accept: application/json"    -H "Content-Type: multipart/form-data"    -F "inputFile=@/home/ubuntu/scripts/output/segments.zip;type=application/zip"    -F "parameters={\"mode\" : \"CREATE_UPDATE\", \"encoding\" : \"UTF-8\", \"format\" : { \"separator\" : \",\", \"endOfLine\" : \"\r\n\", \"enclosing\" : \"\\\"\", \"escaping\" : \"\\\"\" },  \"headerKeyColumn\" : \"actito_id\" };type=application/json"
rm /home/ubuntu/scripts/output/segments.*
echo Sent segments file