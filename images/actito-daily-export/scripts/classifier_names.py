import pandas as pd
import datetime

class Age_Classifier():
    
    def __init__(self, census_stats_file:str='', decades:list=[], publics:list=[]):
        if census_stats_file != '':
            self.df_census=pd.read_csv(census_stats_file, index_col=0)
            self.list_names=self.df_census.index.tolist()
            self.list_years=self.df_census.columns.tolist()
            
            df_census_decades = self.df_census.copy()
            columns = []
            for sett in decades:
                category_columns=[]
                for col_name in sett:
                    if str(col_name) in self.df_census.columns:
                        category_columns.append(str(col_name))
                    #else:
                    #    print('Category not found:', col_name)
                sett_name=category_columns[0] + '-' + category_columns[-1]
                columns.append(sett_name)
                df_census_decades[sett_name]=df_census_decades[category_columns].iloc[:].sum(axis=1)   
            self.df_census_decades = df_census_decades[columns]  

            df_census_publics = self.df_census.copy()
            columns = []
            for sett in publics:
                category_columns=[]
                for col_name in sett:
                    if str(col_name) in self.df_census.columns:
                        category_columns.append(str(col_name))
                    #else:
                    #    print('Category not found:', col_name)
                sett_name=category_columns[0] + '-' + category_columns[-1]
                columns.append(sett_name)
                df_census_publics[sett_name]=df_census_publics[category_columns].iloc[:].sum(axis=1)   
            self.df_census_publics = df_census_publics[columns]            

            
    def _pre_process_name_(self, name:str=''):
        """
        Function to pre process names. Handle exceptions
        """
        if name not in self.list_names:
            # print('Error: name not in data', name)
            return None
        
        return name
            
    def max_year(self, name:str=''):
        """
        return the most likely year for the given name
        """
        name=self._pre_process_name_(name)
        if name == None:
            return
        
        # find and return max
        probs=self.df_census.loc[name]
        pred=probs.idxmax()
        max_probs=probs.max()
        # return pred, max_probs, probs
        return pred, pred, max_probs
    
    
    def max_defined_categories(self,  name:str, type:str):
        """
        """
        name=self._pre_process_name_(name)
        if name == None:
            return
        
        if type == 'decades':
            df = self.df_census_decades
        elif type == 'publics':
            df = self.df_census_publics

        # find and return max
        probs=df.loc[name]
        pred=probs.idxmax()
        max_probs=probs.max()
        keys = pred.split('-')
        # return pred, max_probs, probs
        return keys[0], keys[1], max_probs     
 
    def idxquantile(self, s, q=0.5):
        """
        compute the index of wished quantile (q) within pandas time Series (s)
        if s is distribution over time !
        """
        s=s.cumsum()
        return (s <= q).idxmin()
    
    def inter_quantiles(self, name:str, percentage:float):
        """
        Percentage is wished proportion within population (<1)
        Therefore remove 1-percentage from both right and left tails
        Compute quantile((1-percentage)/2) and quantile(1 -(1-percentage)/2)
        Return index_year of both quantile (min_, max_) and
        remanant proportion between these years
        """
        name=self._pre_process_name_(name)
        if name == None:
            return
        
        if percentage > 1.0:
            print('Error, percentage must be < 1')
            return
        
        probs=self.df_census.loc[name]
        lower_quantile=(1-percentage)/2
        upper_quantile=1-lower_quantile
        
        min_=self.idxquantile(probs, lower_quantile)
        max_=self.idxquantile(probs, upper_quantile)
        
        return min_, max_, probs[min_:max_].sum()