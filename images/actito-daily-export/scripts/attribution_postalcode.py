import pandas as pd
import numpy as np
import unidecode
import datetime
import re
import sqlalchemy as sql
import os
import json
import boto3
import io


def get_today():
    return datetime.datetime.strftime(datetime.datetime.now(), '%y-%m-%d %H:%M:%S')


def parse_to_int(string):
    if string == '':
        return np.nan
    else:
        try:
            return int(string)
        except:
            return string
        
def get_number(string):
    if string == '':
        return np.nan
    else:
        try:
            return int(string)
        except:
            try:
                tmp = re.search('\d{4}', a)
                if tmp:
                    return int(tmp.group(0))
            except:
                return string
        
def clean_string(string, belgium):
    string = string.lower().strip()
    if not belgium:
        string = string.replace('belgium','').strip()
    if ',' in string:
        string = string.split(',')[0]
    if '#####*****' in string:
        vals = string.split('#####*****')
        vals[0] = regex.sub('', unidecode.unidecode(vals[0]).strip()).strip()
        vals[1] = regex.sub('', unidecode.unidecode(vals[1]).strip()).strip()
        string = '{}#####*****{}'.format(vals[0],vals[1])
    else:
        string = unidecode.unidecode(string).strip()
        string = regex.sub('', string).strip()
    return string
        
def count_in_list(obj, _list):
    return sum([1 for x in _list if x == obj])
        
def assign_by_city(city, ucities):
    city_str = clean_string(city, False)
    if city_str in ucities:
        return int(ucities[city_str]['zip'])
    
def assign_by_localite(city, ulocalite):
    city_str = clean_string(city, False)
    if city_str in ulocalite:
        return int(ulocalite[city_str]['zip']) 
    
def assign_by_loc_prov(city, uloc_prov):
    city_str = clean_string(city, False)
    if city_str in uloc_prov:
        return int(uloc_prov[city_str]['zip'])    
    
def assign_country(city, ucountries):
    if ',' in city:
        country = clean_string(city.split(',')[-1], True)
        if country in ucountries:
            return ucountries[country]['code']
        
def assign_ins_by_group(city, uinsgroup):
    city_str = clean_string(city, False)
    if city_str in uinsgroup:
        return int(uinsgroup[city_str]['ins'])        
    
def assign_ins_by_nom(city, uinsnom):
    city_str = clean_string(city, False)
    if city_str in uinsnom:
        return int(uinsnom[city_str]['ins'])       
    
def set_ins(x, ins, zip_ins):
    if x['acquisition'] in acq_zip and int(x['zip']) in zip_ins:
        return zip_ins[int(x['zip'])]
    elif (x['zip'] != x['zip'] or x['zip'] == '') and x['ins'] == x['ins'] and x['ins'] != '' and str(x['ins']).isdigit() and int(x['ins']) in ins:
        return int(x['ins'])
    else:
        return np.nan    

def process_data(_df_users, **kwargs):
    if len(_df_users) == 0:
        return pd.DataFrame(columns=['actitoid', 'zip', 'ins', 'country'])

        
    df_users = _df_users.copy()
    df_users['zip'] = df_users['zip'].apply(lambda x: parse_to_int(x))

    # ---
    # ## 1. Codes naturellement reconnaissables

    df_users['found_zip'] = df_users['zip'].apply(lambda x: 1 if x in kwargs['zips'] else 0)
    df_users['acquisition'] = df_users['found_zip'].apply(lambda x: '1' if x else '')
    df_users['ins'] = 0


    # ---

    # ## 2. Attribution du code postal


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    print('\n{}\nAQC 1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ### 2.1. Par champ "city"


    df_working_ds['zip'] = df_working_ds['city'].apply(lambda x: assign_by_city(str(x), kwargs['ucities']))
    df_working_ds['found_zip'] = df_working_ds['zip'].apply(lambda x: 1 if x in kwargs['zips'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '2.1' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]

    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))

    # ### 2.2. Code postal dans le champ "city"


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['zip'] = df_working_ds['city'].apply(lambda x: get_number(x))
    df_working_ds['found_zip'] = df_working_ds['zip'].apply(lambda x: 1 if x in kwargs['zips'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '2.2' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]

    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ### 2.3 Par champ "city" comme "localité"


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['zip'] = df_working_ds['city'].apply(lambda x: assign_by_localite(str(x), kwargs['ulocalite']))
    df_working_ds['found_zip'] = df_working_ds['zip'].apply(lambda x: 1 if x in kwargs['zips'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '2.3' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]



    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.3\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ### 2.4 Par combination de "Localité, Province"


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['zip'] = df_working_ds['city'].apply(lambda x: assign_by_loc_prov('#####*****'.join(str(x).split(',')),kwargs['uloc_prov']))
    df_working_ds['found_zip'] = df_working_ds['zip'].apply(lambda x: 1 if x in kwargs['zips'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '2.4' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]



    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.4\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ### 2.5 Attribution de code postal par données concours (qualifio)

    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['zip'] = df_working_ds['actitoid'].apply(lambda x: contest[x]['zip'] if x in contest else np.nan)
    df_working_ds_up = df_working_ds.loc[~df_working_ds['zip'].isna()].copy()
    df_working_ds_up['found_zip'] = 1
    df_working_ds_up['acquisition'] = '2.5'

    df_users.update(df_working_ds_up)
    print('\n{}\nAQC 2.5\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ---------


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds = df_working_ds[(df_working_ds['country'].isnull()) | (df_working_ds['country'] == '') | (~(df_working_ds['country'].isnull()) & (df_working_ds['country'] == 'BE'))]


    # ---
    # ## 3. Attribution de code INS


    df_users['zip'] = df_users['zip'].apply(lambda x: x if x in kwargs['zips'] else np.nan)
    df_users['ins'] = _df_users['zip']


    # ### 3.1 Attribution direct de INS par champ "city"


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['ins'] = df_working_ds['city'].apply(lambda x: get_number(x) if get_number(x) in ins else x)
    df_working_ds['found_zip'] = df_working_ds['ins'].apply(lambda x: 1 if x in kwargs['ins'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '3.1' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]



    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ### 3.2 Attribution de INS par champ "city"


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['ins'] = df_working_ds['city'].apply(lambda x: assign_ins_by_nom(str(x), kwargs['uinsnom']))
    df_working_ds['found_zip'] = df_working_ds['ins'].apply(lambda x: 1 if x in kwargs['ins'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '3.2' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]



    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))


    # ### 3.3 Attribution de INS par champ "city" + "Belgium"


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['ins'] = df_working_ds['city'].apply(lambda x: assign_ins_by_group(str(x).replace(', Belgium', '') if ', Belgium' in str(x) else str(x), kwargs['uinsgroup']))
    df_working_ds['found_zip'] = df_working_ds['ins'].apply(lambda x: 1 if str(x) in kwargs['ins'] else 0)
    df_working_ds['acquisition'] = df_working_ds['found_zip'].apply(lambda x: '3.3' if x == 1 else '')
    df_working_ds[df_working_ds['found_zip'] == 1]



    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.3\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))

    # ### 4.1 Attribution de INS a utilisateurs selon leur consommation


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()


    geocons = kwargs['df_geocons'].copy()
    geocons['found_zip'] = 1
    geocons['acquisition'] = '4.1'
    tmp = df_working_ds.reset_index().merge(geocons, how='left', left_on='actitoid', right_on='user', suffixes=['_x','']).set_index('index')


    del tmp['found_zip_x']
    del tmp['acquisition_x']
    del tmp['ins_x']
    df_working_ds.update(tmp)



    df_users.update(df_working_ds)
    print('\n{}\nAQC 4.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found_zip'] == 1])/len(df_users),2), val=len(df_users[df_users['found_zip'] == 1])))

    # ## Attribuer champ "country" pour les codes ZIP trouvés


    df_working_ds = df_users.loc[df_users['found_zip'] == 1].copy()
    df_working_ds['country'] = 'BE'
    df_users.update(df_working_ds)


    # ## Attribuer champ "country" pour les codes ZIP untrouvés


    df_working_ds = df_users.loc[df_users['found_zip'] == 0].copy()
    df_working_ds['country'] = df_working_ds['city'].apply(lambda x: assign_country(str(x) if x == x else '', kwargs['ucountries']))


    # ## Nettoyer output


    df_users['zip'] = df_users.apply(lambda x: np.nan if x['acquisition'] in acq_ins else x['zip'], axis=1)
    df_users['ins'] = df_users.apply(lambda x: set_ins(x, kwargs['ins'], kwargs['zip_ins']), axis=1)
    df_users['country'] = df_users['country'].apply(lambda x: x if x == x else np.nan)


    # ---
    # ## Exporter les résultats
    return df_users[['actitoid', 'zip', 'ins', 'country']]

if __name__=='__main__':

    # ## Variables et constants

    now = datetime.datetime.now()

    ssm = boto3.client('ssm')

    parameter = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)

    engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ['db_username'], parameter['Parameter']['Value'], os.environ['db_host'], os.environ['db_port'], os.environ['db_database']))

    tables = ['actito', 'gigya']

    acq_zip = {'1', '2.1', '2.2', '2.3', '2.4'}
    acq_ins = {'', '3.1', '3.2', '3.3', '4.1'}
    s3 = boto3.client('s3')
    s3_resource = boto3.resource('s3')

    bucket = 'big-data-media'

    # ## Utils

    regex = re.compile('[^a-zA-Z]')    

    # ## Extra constants

    obj_zip = s3.get_object(Bucket=bucket, Key='playground/actito/input/zipcodes.csv')
    
    df_zips = pd.read_csv(io.BytesIO(obj_zip['Body'].read()))
    df_zips.head()

    df_cities = df_zips[['Code postal', 'Commune principale']].drop_duplicates()
    df_cities['Commune principale'] = df_cities['Commune principale'].apply(lambda x: clean_string(x, False))
    df_cities['count'] = df_cities['Commune principale'].apply(lambda x: count_in_list(x, df_cities['Commune principale']))
    df_cities = df_cities.sort_values(by='Commune principale').rename(columns={'Code postal':'zip'})
    ucities = df_cities[df_cities['count'] == 1][['Commune principale', 'zip']]
    ucities = ucities.set_index('Commune principale').to_dict(orient='index')
    ucities

    df_localite = df_zips[['Code postal', 'Localité']].drop_duplicates()
    df_localite['Localité'] = df_localite['Localité'].apply(lambda x: clean_string(x, False))
    df_localite['count'] = df_localite['Localité'].apply(lambda x: count_in_list(x, df_localite['Localité']))
    df_localite = df_localite.sort_values(by='Localité').rename(columns={'Code postal':'zip'})
    ulocalite = df_localite[df_localite['count'] == 1][['Localité', 'zip']]
    ulocalite = ulocalite.set_index('Localité').to_dict(orient='index')
    ulocalite

    df_loc_prov = df_zips.copy()
    df_loc_prov['Localité'] = df_loc_prov['Localité'].apply(lambda x: x.lower().replace('-sur-meuse', '') if '-sur-meuse' in x.lower() else x)
    df_loc_prov['Localité'] = df_loc_prov['Localité'].apply(lambda x: x.lower().replace('-sur-sambre', '') if '-sur-sambre' in x.lower() else x)
    df_loc_prov['loc_prov'] = df_loc_prov.apply(lambda x: '{}#####*****{}'.format(x['Localité'], x['Province']), axis=1)
    df_loc_prov = df_loc_prov[['Code postal', 'loc_prov']].drop_duplicates()
    df_loc_prov['loc_prov'] = df_loc_prov['loc_prov'].apply(lambda x: clean_string(x, False))
    df_loc_prov['count'] = df_loc_prov['loc_prov'].apply(lambda x: count_in_list(x, df_loc_prov['loc_prov']))
    df_loc_prov = df_loc_prov.sort_values(by='loc_prov').rename(columns={'Code postal':'zip'})
    uloc_prov = df_loc_prov[df_loc_prov['count'] == 1][['loc_prov', 'zip']]
    uloc_prov = uloc_prov.set_index('loc_prov').to_dict(orient='index')
    uloc_prov

    obj_country = s3.get_object(Bucket=bucket, Key='playground/actito/input/countrycodes.csv')
    
    df_countries = pd.read_csv(io.BytesIO(obj_country['Body'].read()))
    df_countries['country'] = df_countries['country'].apply(lambda x: clean_string(x, True))
    df_countries['count'] = df_countries['country'].apply(lambda x: count_in_list(x, df_countries['country']))
    df_countries = df_countries.sort_values(by='country')
    ucountries = df_countries[df_countries['count'] == 1][['country','code']]
    ucountries = ucountries.set_index('country').to_dict(orient='index')
    ucountries

    zips = set(df_zips['Code postal'])
    len(zips)

    obj_ins = s3.get_object(Bucket=bucket, Key='playground/actito/input/inscodes.csv')
    
    df_ins = pd.read_csv(io.BytesIO(obj_ins['Body'].read()))
    df_ins.head()

    df_insgroup = df_ins[['ins', 'nom_ins']].drop_duplicates()
    df_insgroup['nom_ins'] = df_insgroup['nom_ins'].apply(lambda x: clean_string(x, False))
    df_insgroup['count'] = df_insgroup['nom_ins'].apply(lambda x: count_in_list(x, df_insgroup['nom_ins']))
    df_insgroup = df_insgroup.sort_values(by='nom_ins')
    uinsgroup = df_insgroup[df_insgroup['count'] == 1][['nom_ins', 'ins']]
    uinsgroup = uinsgroup.set_index('nom_ins').to_dict(orient='index')
    uinsgroup

    df_insobj = df_ins[['ins', 'nom']].drop_duplicates()
    df_insobj['nom'] = df_insobj['nom'].apply(lambda x: clean_string(x, False))
    df_insobj['count'] = df_insobj['nom'].apply(lambda x: count_in_list(x, df_insobj['nom']))
    df_insobj = df_insobj.sort_values(by='nom')
    uinsnom = df_insobj[df_insobj['count'] == 1][['nom', 'ins']]
    uinsnom = uinsnom.set_index('nom').to_dict(orient='index')
    uinsnom

    obj_geocons = s3.get_object(Bucket=bucket, Key='playground/actito/input/geo_users_conso.csv')
    
    df_geocons = pd.read_csv(io.BytesIO(obj_geocons['Body'].read()))

    df_geocons = df_geocons.groupby(by='user', group_keys=False).apply(lambda x: x.loc[x.countoff_appearances.idxmax()])
    df_geocons = df_geocons.reset_index(level=0, drop=True)
    df_geocons = df_geocons[['user','zip']].rename(columns={'zip':'ins'})
    df_geocons.head()

    ins = set(df_ins['ins'])
    len(ins)

    df_zip_ins = df_ins[['ins', 'zip']].groupby(by=['ins', 'zip']).sum().reset_index()
    df_zip_ins = df_zip_ins.set_index('zip')
    zip_ins = df_zip_ins.to_dict()['ins']

    df_contest = pd.read_sql_query("""SELECT actito.actitoid, players.zip, contest.date_from FROM egos_gigya gigya INNER JOIN egos_actito actito ON gigya.actitoid = actito.actitoid INNER JOIN egos_qualifio_players players ON gigya.uid = players.uid INNER JOIN egos_qualifio_contests contest ON players.id_contest = contest.id_contest WHERE players.zip IS NOT NULL AND players.zip <> '' GROUP BY actito.actitoid, players.zip, contest.date_from""", engine)
    df_contest['zip'] = df_contest['zip'].apply(parse_to_int)
    df_contest['zip'] = df_contest['zip'].apply(lambda x: x if x in zips else np.nan)
    df_contest = df_contest[~df_contest['zip'].isna()]
    df_contest = df_contest.sort_values(by=['actitoid','date_from'], ascending=False)
    df_contest = df_contest.drop_duplicates(subset=['actitoid'], keep='first')[['actitoid','zip']]
    df_contest = df_contest.set_index('actitoid')
    contest = df_contest.to_dict(orient='index')

    df_users = pd.read_sql_query("""SELECT actitoid, addr_street, addr_num, addr_box, city, zip, country FROM egos_actito WHERE assignedaddresspostalcode IS NULL OR assignedaddresspostalcode = ''""", engine)

    print('\n{}\nUsers to assign ACTITO ({}): {}\n{}\n'.format(''.join('-'*50),get_today(), len(df_users),''.join('-'*50)))
    data_actito = process_data(df_users, zips=zips, ucities=ucities, ulocalite=ulocalite, uloc_prov=uloc_prov, ins=ins, uinsnom=uinsnom, uinsgroup=uinsgroup, df_geocons=df_geocons, zip_ins=zip_ins, ucountries=ucountries, contest=contest)
    not_null = data_actito[~data_actito['zip'].isna()]['actitoid'].astype(int).astype(str)
    print('\n{}\nProcessed {} ACTITO ({}) users \n{}\n'.format(''.join('-'*50),len(not_null),get_today(),''.join('-'*50)))
    data_actito = data_actito.set_index('actitoid')

    df_users = pd.read_sql_query("""SELECT actito.actitoid, actito.addr_street, actito.addr_num, actito.addr_box, gigya.city, gigya.zip, gigya.country FROM egos_gigya gigya INNER JOIN egos_actito actito ON gigya.actitoid = actito.actitoid WHERE assignedaddresspostalcode IS NULL OR assignedaddresspostalcode = ''""", engine)        
    df_users = df_users[~df_users['actitoid'].isin(not_null)]
    df_users = df_users.sort_values(by=['city','zip','country'], ascending=False)
    df_users = df_users.drop_duplicates(subset=['actitoid'], keep='first')
    print('\n{}\nAdditional users from GIGYA ({}) to assign: {}\n{}\n'.format(''.join('-'*50),get_today(), len(df_users),''.join('-'*50)))
    data_gigya = process_data(df_users, zips=zips, ucities=ucities, ulocalite=ulocalite, uloc_prov=uloc_prov, ins=ins, uinsnom=uinsnom, uinsgroup=uinsgroup, df_geocons=df_geocons, zip_ins=zip_ins, ucountries=ucountries, contest=contest)
    print('\n{}\nProcessed {} GIGYA ({}) users\n{}\n'.format(''.join('-'*50),len(data_gigya),get_today(),''.join('-'*50)))
    data_gigya = data_gigya.set_index('actitoid')

    data_actito.update(data_gigya)

    data_actito['AssignedaddressPostalCode'] = data_actito.apply(lambda row: row['zip'] if row['zip'] == row['zip'] else row['ins'] if row['ins'] == row['ins'] else np.nan, axis=1)
    data_actito['AssignedaddressCountry'] = data_actito['country']

    data_actito = data_actito.reset_index()

    try:

        csv_buffer = io.StringIO()
        data_actito = data_actito[~data_actito['AssignedaddressPostalCode'].isna()].rename(columns={'actitoid':'profileId'})
        data_actito['profileId'] = data_actito['profileId'].astype(int)
        data_actito = data_actito[['profileId', 'AssignedaddressPostalCode', 'AssignedaddressCountry']]
        data_actito.to_csv(csv_buffer, index=False, encoding='utf-8-sig')
        data_actito.to_csv('/home/ubuntu/scripts/output/attribution_codepostal.csv', index=False, encoding='utf-8-sig')
        response = s3_resource.Object(bucket, 'playground/actito/output/{}/{}/{}/attribution_codepostal.csv'.format(datetime.datetime.strftime(now,'%Y'),datetime.datetime.strftime(now,'%m'),datetime.datetime.strftime(now,'%d'))).put(Body=csv_buffer.getvalue())
        print({
            'statusCode': response['ResponseMetadata']['HTTPStatusCode'],
            'body': json.dumps('{} : {} attribution_codepostal.csv'.format(get_today(), 'Saved file' if response['ResponseMetadata']['HTTPStatusCode'] == 200 else 'Could not write file'))
        })
    except Exception as e:
        print({
            'statusCode': 500,
            'body': json.dumps('{} : Could not write file attribution_codepostal.csv {}'.format(get_today(), str(e)))
        })