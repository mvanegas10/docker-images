#!/usr/bin/env python
# coding: utf-8


import pandas as pd
import numpy as np
import unidecode
import datetime
import random
import classifier_names as cn
import re
import sqlalchemy as sql
import os
import json
import boto3
import io

# ## Utils

def get_today():
    return datetime.datetime.strftime(datetime.datetime.now(), '%y-%m-%d %H:%M:%S')


def to_camel_case(string):
    string = string.strip()
    if ' ' in  string:
        names = []
        for x in string.split(' '):
            x = x.strip()
            names.append('{}{}'.format(x[:1].upper(), x[1:].lower()))
        return ' '.join(names)
    elif '-' in  string:
        names = []
        for x in string.split('-'):
            x = x.strip()
            names.append('{}{}'.format(x[:1].upper(), x[1:].lower()))
        return '-'.join(names)
    else:
        return '{}{}'.format(string[:1].upper(), string[1:].lower())



def to_lower_strip(string):
    string = string.lower().strip()
    return string


def rm_sc(string, regex):
    string = string.lower().strip()
    string = unidecode.unidecode(string).strip()
    string = regex.sub('', string).strip()
    return string


def remove_special_chars(string, regex):
    if not '�' in string:
        strings = string.split('-')
        return '-'.join([rm_sc(x, regex) for x in strings])
    else:
        return string


def remove_email_domain(string):
    if '@' in string:
        return to_lower_strip(string.split('@')[0])
    

def create_substring(row, col, **kwargs):
    length = len(row[col]) if row[col] != None else 0
    
    for i in range(length-1):
        name = row[col][0:length-i]
        if name in kwargs['firstnames_wo_sc']:
            row['found'] = 1
            row['accepted_firstname'] = kwargs['firstnames_wo_sc'][name]['firstname']
            row['possible_genders'] = assign_possible_genders(name, kwargs['firstnames_wo_sc'])
            return row
        elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
            row['found'] = 1
            row['accepted_firstname'] = kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname']
            row['possible_genders'] = assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc'])
            return row            
        
    for i in range(2, length):
        name = row[col][i:length]
        if name in kwargs['firstnames_wo_sc']:
            row['found'] = 1
            row['accepted_firstname'] = kwargs['firstnames_wo_sc'][name]['firstname']
            row['possible_genders'] = assign_possible_genders(name, kwargs['firstnames_wo_sc'])
            return row
        elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
            row['found'] = 1
            row['accepted_firstname'] = kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname']
            row['possible_genders'] = assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc'])
            return row         
        
    return row


def find_substrings(row, col, **kwargs):
    string = row[col] if row[col] != None else ''
    accepted = []
    genders = []
    age_method_1 = []
    age_method_2 = []
    age_method_3 = []
    
    if ' ' in string:
        for name in string.split(' '):
            if name in kwargs['firstnames']:
                accepted.append(kwargs['firstnames'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif name in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname'])
                genders.append(assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            
    if '-' in string:
        for name in string.split('-'):
            if name in kwargs['firstnames']:
                accepted.append(kwargs['firstnames'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif name in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname'])
                genders.append(assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            
    if '.' in string:
        for name in string.split('.'):
            if name in kwargs['firstnames']:
                accepted.append(kwargs['firstnames'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif name in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname'])  
                genders.append(assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            
    if '_' in string:
        for name in string.split('_'):
            if name in kwargs['firstnames']:
                accepted.append(kwargs['firstnames'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif name in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname'])
                genders.append(assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
                
    if '#' in string:
        for name in string.split('#'):
            if name in kwargs['firstnames']:
                accepted.append(kwargs['firstnames'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif name in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][name]['firstname'])
                genders.append(assign_possible_genders(name, kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(name, kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(name, kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(name, kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                
            elif remove_special_chars(name, kwargs['regex']) in kwargs['firstnames_wo_sc']:
                accepted.append(kwargs['firstnames_wo_sc'][remove_special_chars(name, kwargs['regex'])]['firstname'])                
                genders.append(assign_possible_genders(remove_special_chars(name, kwargs['regex']), kwargs['firstnames_wo_sc']))
                age_method_1.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 1, kwargs['classifier_obj']))
                age_method_2.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 2, kwargs['classifier_obj']))
                age_method_3.append(predict_age(remove_special_chars(name, kwargs['regex']), kwargs['age_classifier'], 3, kwargs['classifier_obj']))                                                

    if len(accepted) > 0:
        row['found'] = 1
        row['accepted_firstname'] = ' '.join(accepted)
        row['possible_genders'] = ','.join(genders)
        row['assignedyear'] = choose_age_range(age_method_1)
        row['assigneddeceny'] = choose_age_range(age_method_2)
        row['assignedpublic'] = choose_age_range(age_method_3)
    return row


def assign_possible_genders(name, obj):
    rnd = random.random()
    if rnd < obj[name]['m']:
        return 'm'
    else:
        return 'f'

    
def predict_age(name, age_classifier, method, classifier_obj):
    if method == 1:
        return age_classifier.max_year(name.upper())
    elif method == 2:
        return age_classifier.max_defined_categories(name.upper(), 'decades')
    elif method == 3:
        return age_classifier.max_defined_categories(name.upper(), 'publics')


def get_all_age_predictions(row, age_classifier, classifier_obj):
    row['assignedyear'] = predict_age(row['accepted_firstname'], age_classifier, 1, classifier_obj)
    row['assigneddeceny'] = predict_age(row['accepted_firstname'], age_classifier, 2, classifier_obj)
    row['assignedpublic'] = predict_age(row['accepted_firstname'], age_classifier, 3, classifier_obj)
    return row


def choose_age_range(pred_list):
    max_prob = 0
    list_idx = 0
    for i, triple in enumerate(pred_list):
        if triple is not None and max_prob < triple[2]:
            max_prob = triple[2]
            list_idx = i
    return pred_list[list_idx]


def choose_gender(string):
    if string == string and ',' in string:
        values = string.split(',')
        m = sum([1 for m in values if m == 'm'])
        f = sum([1 for f in values if f == 'f'])
        if m > f:
            return 'm'
        elif m < f:
            return 'f'
        else:
            return np.nan
    else:
        return string


def process_data(df_users, **kwargs):
    if len(df_users) == 0:
        return pd.DataFrame(columns=['actitoid', 'assignedfirstname', 'assignedlastname', 'acquisition', 'possible_genders', 'assignedyear', 'assigneddeceny', 'assignedpublic'])
    # ### Utilisateurs

    df_users['original_firstname'] = df_users['firstname']
    df_users['original_lastname'] = df_users['lastname']
    df_users['firstname'] = df_users['firstname'].astype(str)
    df_users['lastname'] = df_users['lastname'].astype(str)
    df_users['found'] = 0
    df_users['accepted_firstname'] = ''
    df_users['acquisition'] = ''
    df_users['possible_genders'] = ''
    df_users['assignedyear'] = np.nan
    df_users['assigneddeceny'] = np.nan
    df_users['assignedpublic'] = np.nan
    #df_users['assignedyears'] = np.nan


    df_users['firstname'] = df_users['firstname'].apply(lambda x: to_lower_strip(x))
    df_users['lastname'] = df_users['lastname'].apply(lambda x: to_lower_strip(x))


    # ### 1.1 Attribution de prénom à partir du champ "firstname"


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: kwargs['firstnames'][x]['firstname'] if x in kwargs['firstnames'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if x != '' else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '1.1'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['firstname'].apply(lambda x: assign_possible_genders(x, kwargs['firstnames']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 1.2 Attribution de prénom à partir du champ "firstname" en joignant les mots par un tiret


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: kwargs['firstnames']['-'.join(x.split(' '))]['firstname'] if '-'.join(x.split(' ')) in kwargs['firstnames'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if x != '' else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '1.2'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['firstname'].apply(lambda x: assign_possible_genders('-'.join(x.split(' ')), kwargs['firstnames']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 1.3 Attribution de prénom à partir du champ "firstname" + "lastname"


    df_working_ds = df_users.loc[(df_users['found'] == 0)].copy()

    df_working_ds['accepted_firstname'] = df_working_ds.apply(lambda x: kwargs['firstnames']['{}{}'.format(x['firstname'], x['lastname'])]['firstname'] if '{}{}'.format(x['firstname'], x['lastname']) in kwargs['firstnames'] else '', axis=1)
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '1.3'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender.apply(lambda x: assign_possible_genders('{}{}'.format(x['firstname'], x['lastname']), kwargs['firstnames']), axis=1)
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.3\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))

    # ### 1.4 Attribution de prénom à partir du champ "lastname"


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['lastname'].apply(lambda x: kwargs['firstnames'][x]['firstname'] if x in kwargs['firstnames'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '1.4'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['lastname'].apply(lambda x: assign_possible_genders(x, kwargs['firstnames']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 1.4\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 2.1 Attribution du prénom à partir de champ "firstname" en supprimant les caractères spéciaux


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: kwargs['firstnames_wo_sc'][x]['firstname'] if x in kwargs['firstnames_wo_sc'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '2.1'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['firstname'].apply(lambda x: assign_possible_genders(x, kwargs['firstnames_wo_sc']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 2.2 Attribution du prénom à partir de champ "lastname" en supprimant les caractères spéciaux


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['lastname'].apply(lambda x: kwargs['firstnames_wo_sc'][x]['firstname'] if x in kwargs['firstnames_wo_sc'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 3 else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '2.2'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['lastname'].apply(lambda x: assign_possible_genders(x, kwargs['firstnames_wo_sc']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 2.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 3.1 Attribution du prénom à partir de champ "firstname" en supprimant les caractères spéciaux


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: kwargs['firstnames_wo_sc'][remove_special_chars(x, kwargs['regex'])]['firstname'] if remove_special_chars(x, kwargs['regex']) in kwargs['firstnames_wo_sc'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '3.1'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['firstname'].apply(lambda x: assign_possible_genders(remove_special_chars(x, kwargs['regex']), kwargs['firstnames_wo_sc']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 3.2 Attribution du prénom à partir de champ "lastname" en supprimant les caractères spéciaux


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')].copy()

    df_working_ds['accepted_firstname'] = df_working_ds['lastname'].apply(lambda x: kwargs['firstnames_wo_sc'][remove_special_chars(x, kwargs['regex'])]['firstname'] if remove_special_chars(x, kwargs['regex']) in kwargs['firstnames_wo_sc'] else '')
    df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if len(x) > 2 else 0)
    df_to_update = df_working_ds.loc[df_working_ds['found'] == 1].copy()
    df_to_update['acquisition'] = '3.2'
    df_to_update_gender = df_to_update.loc[df_to_update['gender'].isna()].copy()
    df_to_update_gender['possible_genders'] = df_to_update_gender['lastname'].apply(lambda x: assign_possible_genders(remove_special_chars(x, kwargs['regex']), kwargs['firstnames_wo_sc']))
    df_to_update.update(df_to_update_gender)
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_working_ds.update(df_to_update)

    df_users.update(df_working_ds)
    print('\n{}\nAQC 3.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 4.1 Attribution du prénom à partir de un des mots dans le champ "firstname" 


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')].copy()

    df_working_ds = df_working_ds.apply(lambda x: find_substrings(x, 'firstname', **kwargs), axis=1)
    df_to_update = df_working_ds.loc[df_working_ds['accepted_firstname'].str.len() > 3].copy()
    df_to_update['acquisition'] = '4.1'
    df_to_update_gender = df_to_update.loc[(~df_to_update['gender'].isna())].copy()
    df_to_update_gender['possible_genders'] = ''
    df_to_update.update(df_to_update_gender)    
    df_working_ds = df_to_update.copy()
    df_users.update(df_working_ds)
    print('\n{}\nAQC 4.1\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 4.2 Attribution du prénom à partir de un des mots dans le champ "lastname" 


    df_working_ds = df_users.loc[(df_users['found'] == 0) & (df_users['lastname'] != 'nan')].copy()

    df_working_ds = df_working_ds.apply(lambda x: find_substrings(x, 'lastname', **kwargs), axis=1)
    df_to_update = df_working_ds.loc[df_working_ds['accepted_firstname'].str.len() > 3].copy()
    df_to_update['acquisition'] = '4.2'
    df_to_update_gender = df_to_update.loc[(~df_to_update['gender'].isna())].copy()
    df_to_update_gender['possible_genders'] = ''
    df_to_update.update(df_to_update_gender)     
    df_working_ds = df_to_update.copy()
    df_users.update(df_working_ds)
    print('\n{}\nAQC 4.2\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ### 5 Attribution du prénom à partir de un des mots dans le champ "email" 


    df_working_ds = df_users.loc[df_users['found'] == 0].copy()

    df_working_ds['tmp'] = df_working_ds['email'].apply(lambda x: remove_email_domain(str(x)))
    df_working_ds = df_working_ds.apply(lambda x: find_substrings(x, 'tmp', **kwargs), axis=1)
    del df_working_ds['tmp']
    df_to_update = df_working_ds.loc[df_working_ds['accepted_firstname'].str.len() > 3].copy()
    df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    df_to_update['acquisition'] = '5'
    df_to_update_gender = df_to_update.loc[(~df_to_update['gender'].isna())].copy()
    df_to_update_gender['possible_genders'] = ''
    df_to_update.update(df_to_update_gender)   
    df_working_ds = df_to_update.copy()
    df_users.update(df_working_ds)
    print('\n{}\nAQC 5\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # # ### 6 Attribution du prénom à partir de un sous-chaîne du champ "email" 


    # df_working_ds = df_users[df_users['found'] == 0]

    # df_working_ds['tmp'] = df_working_ds['email'].apply(lambda x: remove_email_domain(str(x)))
    # df_working_ds = df_working_ds.apply(lambda x: create_substring(x, 'tmp', **kwargs), axis=1)
    # del df_working_ds['tmp']
    # df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    # df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    # df_to_update['acquisition'] = '6'
    # df_working_ds = df_to_update.copy()

    # df_users.update(df_working_ds)
    # print('\n{}\nAQC 6\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # # ### 7 Attribution de prénom à partir du champ "firstname" avec erreurs de codification


    # df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    # df_working_ds['accepted_firstname'] = df_working_ds['firstname'].apply(lambda x: kwargs['firstnames_er_sc'][re.sub('[^A-Za-z0-9]+', '', x)]['firstname'] if re.sub('[^A-Za-z0-9]+', '', x) in kwargs['firstnames_er_sc'] and len(kwargs['firstnames_er_sc'][re.sub('[^A-Za-z0-9]+', '', x)]['firstname']) == len(x) else '')
    # df_working_ds['found'] = df_working_ds['accepted_firstname'].apply(lambda x: 1 if x != '' else 0)
    # df_to_update = df_working_ds[df_working_ds['found'] == 1]
    # df_to_update['possible_genders'] = df_to_update['firstname'].apply(lambda x: assign_possible_genders(re.sub('[^A-Za-z0-9]+', '', x), kwargs['firstnames_er_sc']))
    # df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    # df_to_update['acquisition'] = '7'
    # df_working_ds.update(df_to_update)

    # df_users.update(df_working_ds)
    # print('\n{}\nAQC 7\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # # ### 8 Attribution du prénom à partir de un sous-chaîne du champ "firstname" 


    # df_working_ds = df_users[(df_users['found'] == 0) & (df_users['firstname'] != 'nan')]

    # df_working_ds = df_working_ds.apply(lambda x: create_substring(x, 'firstname', **kwargs), axis=1)
    # df_to_update = df_working_ds[df_working_ds['accepted_firstname'].str.len() > 3]
    # df_to_update = df_to_update.apply(lambda row: get_all_age_predictions(row, kwargs['age_classifier'], kwargs['classifier_obj']), axis=1)
    # df_to_update['found'] = 1
    # df_to_update['acquisition'] = '8'
    # df_working_ds = df_to_update.copy()

    # df_users.update(df_working_ds)
    # print('\n{}\nAQC 8\n{}\n{}% ({val:,})\n'.format(''.join('-'*50), ''.join('-'*50), round(100*len(df_users[df_users['found'] == 1])/len(df_users),2), val=len(df_users[df_users['found'] == 1])))


    # ---

    # ## Remplacer valeurs originals pour les champs "firstname" et "lastname"
    # Les champs "original_firstname" et "original_lastname" préservent les valeurs originals


    df_fr_lastname = df_users.loc[df_users['acquisition'].isin(['1.4', '2.2', '3.2', '4.2'])].copy()
    df_fr_lastname['lastname'] = df_fr_lastname['firstname']
    df_users.update(df_fr_lastname)
    df_fr_lastname.head()



    df_users['assignedfirstname'] = df_users['accepted_firstname']
    df_users['assignedlastname'] = df_users['lastname'].apply(to_camel_case)



    tmp = df_users.loc[df_users['found'] == 0].copy()
    tmp['assignedfirstname'] = ''
    tmp['assignedlastname'] = ''
    df_users.update(tmp)



    tmp = df_users.loc[df_users['firstname'] == 'Nan'].copy()
    tmp['assignedfirstname'] = ''
    df_users.update(tmp)



    tmp = df_users.loc[df_users['lastname'] == 'Nan'].copy()
    tmp['assignedlastname'] = ''
    df_users.update(tmp)


    return df_users[['actitoid', 'assignedfirstname', 'assignedlastname', 'acquisition', 'possible_genders', 'assignedyear', 'assigneddeceny', 'assignedpublic']]


if __name__=='__main__':

    # ## Variables et constants

    # ## Config

    ssm = boto3.client('ssm')

    parameter = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)

    engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ['db_username'], parameter['Parameter']['Value'], os.environ['db_host'], os.environ['db_port'], os.environ['db_database']))

    this_year=datetime.datetime.now().year

    interval_cum_percent = float(os.environ['interval_cum_percent'])
    
    list_decades=[]
    for i in json.loads(os.environ['in_list_decades']):
        list_decades.append(list(range(this_year-i, this_year-(i-10))))
    

    groups_rtbf=json.loads(os.environ['in_list_publics'])
    list_intervalls=[]
    for group in groups_rtbf:
        list_intervalls.append(list(range(this_year-group[1], this_year-group[0])))

    ids = {'actito': 'actitoid', 'gigya': 'uid'}
    regex = re.compile('[^a-zA-Z]')

    table = 'actito'
    s3 = boto3.client('s3')
    s3_resource = boto3.resource('s3')

    bucket = 'big-data-media'

    obj_names_proba = s3.get_object(Bucket=bucket, Key='playground/actito/input/names_proba_db.csv')

    # ## Données

    # ### Prénoms provenant de [Statbel](https://statbel.fgov.be/fr/themes/population/noms-et-prenoms/prenoms-filles-et-garcons#panel-13) et [Insee](https://www.insee.fr/fr/statistiques/2540004)

    obj_sb = s3.get_object(Bucket=bucket, Key='playground/actito/input/names_gender_be.csv')
    
    df_firstnames_sb = pd.read_csv(io.BytesIO(obj_sb['Body'].read()))
    df_firstnames_sb['source'] = 'statbel'
    df_firstnames_sb.head()

    obj_in = s3.get_object(Bucket=bucket, Key='playground/actito/input/names_gender_fr.csv')

    df_firstnames_in = pd.read_csv(io.BytesIO(obj_in['Body'].read()))
    df_firstnames_in['source'] = 'insee'
    df_firstnames_in.head()


    df_firstnames = df_firstnames_in.append(df_firstnames_sb, ignore_index=True)
    df_firstnames['m'] = df_firstnames['m'].apply(lambda x: x if x == x else 0)
    df_firstnames['f'] = df_firstnames['f'].apply(lambda x: x if x == x else 0)
    df_firstnames = df_firstnames[df_firstnames['firstname'] != 'Nan']
    df_firstnames.shape



    df_firstnames['total'] = df_firstnames['m'] + df_firstnames['f']
    df_firstnames['m'] = df_firstnames['m']/df_firstnames['total']
    df_firstnames['f'] = df_firstnames['f']/df_firstnames['total']
    del df_firstnames['total']
    df_firstnames.head()



    firstnames = {}
    for i, row in df_firstnames.iterrows():
        if row['firstname'] == row['firstname']:
            firstnames[to_lower_strip(row['firstname'])] = row.to_dict()



    firstnames_wo_sc = {}
    for i, row in df_firstnames.iterrows():
        if row['firstname'] == row['firstname']:
            firstnames_wo_sc[remove_special_chars(row['firstname'], regex)] = row.to_dict()



    firstnames_er_sc = {}
    for i, row in df_firstnames.iterrows():
        if row['firstname'] == row['firstname']:
            firstnames_er_sc['{}'.format(re.sub('[^A-Za-z0-9]+', '', to_lower_strip(row['firstname'])))] = row.to_dict()

    classifier_obj = {
        'list_deceny': list_decades,
        'list_intervalls': list_intervalls,
        'interval_cum_percent': interval_cum_percent
    }

    age_classifier = cn.Age_Classifier(census_stats_file=io.BytesIO(obj_names_proba['Body'].read()), decades=list_decades, publics=list_intervalls)

    df = pd.DataFrame()
    now = datetime.datetime.now()

    df_users = pd.read_sql_query("""SELECT actitoid, email, firstname, lastname, UPPER(gender) as gender FROM egos_actito WHERE assignedfirstname IS NULL OR assignedfirstname = ''""", engine)
    df_users['gender'] = df_users['gender'].apply(lambda x: x.strip() if x is not None and x == x and x in ['M','F'] else np.nan)
    print('\n{}\nUsers to assign ACTITO ({}): {}\n{}\n'.format(''.join('-'*50), get_today(), len(df_users),''.join('-'*50)))
    data_actito = process_data(df_users, regex=regex, age_classifier=age_classifier, classifier_obj=classifier_obj, firstnames=firstnames, firstnames_wo_sc=firstnames_wo_sc, firstnames_er_sc=firstnames_er_sc)
    not_null = list(data_actito[data_actito['assignedfirstname'] != '']['actitoid'].astype(int).astype(str))
    print('\n{}\nProcessed {} ACTITO users \n{}\n'.format(''.join('-'*50),len(not_null),''.join('-'*50)))
    data_actito = data_actito.set_index('actitoid')

    df_users = pd.read_sql_query("""SELECT gigya.actitoid, gigya.email, gigya.firstname, gigya.lastname, NULL as gender FROM egos_gigya gigya INNER JOIN egos_actito actito ON gigya.actitoid = actito.actitoid WHERE assignedfirstname IS NULL OR assignedfirstname = ''""", engine)
    df_users['gender'] = df_users['gender'].apply(lambda x: x.strip() if x is not None and x == x and x in ['M','F'] else np.nan)
    df_users = df_users[~df_users['actitoid'].isin(not_null)]
    df_users = df_users.sort_values(by=['firstname','lastname','email'], ascending=False)
    df_users = df_users.drop_duplicates(subset=['actitoid'], keep='first')
    print('\n{}\nAdditional users from GIGYA to assign ({}): {}\n{}\n'.format(''.join('-'*50), get_today(), len(df_users),''.join('-'*50)))
    data_gigya = process_data(df_users, regex=regex, age_classifier=age_classifier, classifier_obj=classifier_obj, firstnames=firstnames, firstnames_wo_sc=firstnames_wo_sc, firstnames_er_sc=firstnames_er_sc)
    print('\n{}\nProcessed {} GIGYA users\n{}\n'.format(''.join('-'*50),len(data_gigya),''.join('-'*50)))
    data_gigya = data_gigya.set_index('actitoid')

    data_actito.update(data_gigya)
    df = data_actito.copy().reset_index()

    df['assignedgender'] = df['possible_genders'].apply(choose_gender)

    df['AssignedBirthYear'] = df['assignedyear'].apply(lambda x: int(x[0]) if x == x and x is not None and len(x) > 0 else np.nan)
    df['AssignedBirthDeceny'] = df['assigneddeceny'].apply(lambda x: '{}-{}'.format(int(x[0]), int(x[1])) if x == x and x is not None and len(x) > 0 else np.nan)
    df['AssignedPublic'] = df['assignedpublic'].apply(lambda x: '{}-{}'.format(int(x[0]), int(x[1])) if x == x and x is not None and len(x) > 0 else np.nan)

    try:

        csv_buffer = io.StringIO()
        df = df.rename(columns={ids[table]: 'profileId', 'assignedfirstname': 'AssignedFirstname', 'assignedlastname': 'AssignedLastname', 'assignedgender': 'AssignedGender'})
        df['profileId'] = df['profileId'].astype(int)
        df = df[['profileId', 'AssignedFirstname', 'AssignedLastname', 'AssignedGender', 'AssignedBirthYear', 'AssignedBirthDeceny', 'AssignedPublic']]
        df.to_csv(csv_buffer, index=False, encoding='utf-8-sig')
        df.to_csv(os.path.join('/home/ubuntu/scripts/output/attribution_firstname.csv'), index=False, encoding='utf-8-sig')
        response = s3_resource.Object(bucket, 'playground/actito/output/{}/{}/{}/attribution_firstname.csv'.format(datetime.datetime.strftime(now,'%Y'),datetime.datetime.strftime(now,'%m'),datetime.datetime.strftime(now,'%d'))).put(Body=csv_buffer.getvalue())

        print({

            'statusCode': response['ResponseMetadata']['HTTPStatusCode'],
            'body': json.dumps('{} : {} attribution_firstname.csv'.format(get_today(), 'Saved file' if response['ResponseMetadata']['HTTPStatusCode'] == 200 else 'Could not write file'))
        })
    except Exception as e:
        print({
            'statusCode': 500,
            'body': json.dumps('{} : Could not write file attribution_firstname.csv {}'.format(get_today(), str(e)))
        })