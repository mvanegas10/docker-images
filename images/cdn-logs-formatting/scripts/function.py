#!/usr/bin/env python
# coding: utf-8

# -*- coding: utf-8 -*-
import datetime
import io
import boto3
import gzip
import dateutil.parser
import pandas as pd
import numpy as np
import sys
import json
import configparser


def get_int(string):
    try:
        int(string)
        return string
    except Exception as e:
        return np.nan


def get_split_key(string, char, i):
    result = np.nan
    values = string.split(char)
    if i < len(values):
        result = values[i]
    return result        


if __name__=='__main__':
    try:
        config_file = sys.argv[1]
        
        # Configuration parser
        config = configparser.ConfigParser()
        config.read('/home/ubuntu/scripts/{}.ini'.format(config_file))

        # Internal AWS resources

        ssm = boto3.client('ssm')
        s3 = boto3.client('s3')
        s3_resource = boto3.resource('s3')    

        bucket = config['DEFAULT']['BUCKET']
        prefix = config['DEFAULT']['KEY']
        column_labels = ['date','time','ip','method','uri','status','bytes','time-taken','referer','user_agent','cookie','custom','log_group','dsid','extstatus','headersize','cache','tcwait','tcpinfo_rttvar','tcpinfo_snd_cwnd','tcpinfo_rcv_space','tdwait','col22','col23']

        paginator = s3.get_paginator('list_objects_v2')
        pages = paginator.paginate(Bucket=bucket, Prefix=prefix)
        files = []
        for page in pages:
            files.extend([obj['Key'] for obj in page['Contents']])

        all_files = {}
        for obj in files:
            if obj[len(obj)-7:] == '.log.gz':
                all_files[obj] = {
                    'datetime' : dateutil.parser.isoparse(obj.split('/')[5][:23]).replace(minute=0, second=0, microsecond=0) 
                }

        if config['DEFAULT']['TYPE'] == 'full':
            datetimes = set([x['datetime'] for x in all_files.values()])
        elif config['DEFAULT']['TYPE'] == 'day':
            day = datetime.datetime.now() - datetime.timedelta(1)
            datetimes.append(day)


        for date in datetimes:    
            df = pd.DataFrame()
            for key, value in all_files.items():
                if value['datetime'] == date:
                    data = s3.get_object(Bucket=bucket, Key=key)
                    df = df.append(pd.read_csv(gzip.GzipFile(None,'rb',fileobj=io.BytesIO(data['Body'].read())), sep='\t', header=None, encoding='utf-8-sig'), ignore_index=True)
            df.columns = column_labels
            df.to_parquet('s3://{}/{}/formatted/{}.log.snappy.parquet'.format(bucket, prefix, date.strftime('%Y/%m/%d/%Y%d%dT%H:00:00.000')), compression='snappy', index=False)

        print({
            'statusCode': 200,
            'body': json.dumps('{} : Finished formatting files.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))
        })

    except Exception as e:
        print({
            'statusCode': 500,
            'body': json.dumps('{} : Error formatting files {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), e))
        })