kubeless -n oaos function delete createurs-contenus
kubeless -n oaos function deploy createurs-contenus --runtime python3.8 \
                                --from-file function.py \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --dependencies requirements.txt \
                                --timeout 3600 \
                                --handler function.handler

kubeless -n oaos trigger cronjob create \
  createurs-contenus-daily \
  --function createurs-contenus \
  --payload-from-file payload_daily.json \
  --schedule "0 4 * * *"