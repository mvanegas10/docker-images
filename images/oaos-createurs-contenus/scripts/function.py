#!/usr/bin/env python
# coding: utf-8

# ## Importations
import pandas as pd
import numpy as np
import sqlalchemy as sql
import io
import os
import sys
import boto3
import json
import datetime
from botocore.config import Config
from webdav3.client import Client



if __name__=='__main__':

    try:

        # Constants 

        CHARSET = 'UTF-8'

        ssm = boto3.client('ssm')
        s3 = boto3.client('s3')
        s3_resource = boto3.resource('s3')    
        schema = 'public'
        table = 'article_content_creation'

        # Configuration

        pass_redshift = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)

        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ['db_username'], pass_redshift['Parameter']['Value'], os.environ['db_host'], os.environ['db_port'], os.environ['db_database']))

        options_webdav = {
            'webdav_hostname': os.environ['webdavhost'],
            'webdav_login': os.environ['webdavuser'],
            'webdav_password': os.environ['WEBDAV_PASS']
        }        

        webdav = Client(options_webdav)
        webdav.download_sync(remote_path=os.environ['webdav_path'], local_path=os.environ['local_file_path'])

        df = pd.read_excel(os.environ['local_file_path'], engine='openpyxl')

        df = df[['source', 'activite', 'type_source', 'statut', 'cellule', 'thematique','nom']]
        with engine.begin() as conn:
            df.to_sql(name=table, schema=schema, con=conn, method='multi', if_exists='replace', index=False, dtype={'source': sql.types.String,'activite': sql.types.String,'type_source': sql.types.String,'statut': sql.types.String,'cellule': sql.types.String,'thematique': sql.types.String,'nom': sql.types.String})        

        df_new_sources = pd.read_sql("""
            WITH q1 AS
                (SELECT COUNT(*)*1.0 as cnt
                FROM article_article
                WHERE published = '1' AND displaydate <= CURRENT_DATE AND displaydate >= DATE_ADD('year',-1,CURRENT_DATE))
                SELECT TRIM(LOWER(source)) AS s, COUNT(*)/q1.cnt AS pourcentage_last_year
                FROM article_article,q1
                WHERE published='1' AND displaydate <= CURRENT_DATE AND displaydate > DATE_ADD('year',-1,CURRENT_DATE) 
                    AND source NOT ILIKE '%%belga%%'
                    AND source NOT ILIKE '%%afp%%'
                GROUP BY s, q1.cnt
                ORDER BY pourcentage_last_year DESC 
        """, engine)
        df_new_sources = df_new_sources.rename(columns={'s':'source'})

        df_new_sources['cummule'] = df_new_sources['pourcentage_last_year'].cumsum()

        df = df.merge(df_new_sources, how='outer')
        df = df.sort_values('pourcentage_last_year', ascending=False)
        df = df.set_index('source')
        df = df[~df.isna().all(axis=1)]
        df = df.reset_index()        

        df.to_excel(os.environ['local_file_path'], index=False)
        webdav.upload_sync(remote_path=os.environ['webdav_path'], local_path=os.environ['local_file_path'])

        print({
            'statusCode': 200,
            'body': json.dumps('{} : Finished generating file.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))
        })  

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        print('{}\n{}\n{}\n'.format(
            'statusCode',
            json.dumps('{} : Error generating file {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), '{}, {}, {}'.format(exc_type, fname, exc_tb.tb_lineno))),
            e
        ))  