kubeless function delete get-article-videos-metadata
kubeless function deploy get-article-videos-metadata --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env db_database=a360 \
                                --env db_host=10.200.0.84 \
                                --env db_port=5439 \
                                --env db_username=meiv \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --timeout 432000 \
                                --handler function.handler

kubeless trigger cronjob create \
  get-article-videos-metadata-daily \
  --function get-article-videos-metadata \
  --schedule "3 * * * *"                                