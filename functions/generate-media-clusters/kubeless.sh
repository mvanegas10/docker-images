kubeless function delete generate-media-clusters
kubeless function deploy generate-media-clusters --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env DB_DB=a360 \
                                --env DB_HOST=10.200.0.84 \
                                --env DB_PORT=5439 \
                                --env DB_USER=meiv \
                                --env RECO_DEV_DB=bdd_reco \
                                --env RECO_DEV_HOST=database-1.cluster-cmewnkhytu1o.eu-central-1.rds.amazonaws.com \
                                --env RECO_DEV_PORT=5432 \
                                --env RECO_DEV_USER=postgres \
                                --env RECO_PROD_DB=bdm \
                                --env RECO_PROD_HOST=reco-system-prod.cluster-cmewnkhytu1o.eu-central-1.rds.amazonaws.com \
                                --env RECO_PROD_PORT=5432 \
                                --env RECO_PROD_USER=postgres \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --timeout 3600 \
                                --handler function.handler

kubeless trigger cronjob create \
  generate-media-clusters-prod \
  --function generate-media-clusters \
  --payload-from-file payload_prod.json \
  --schedule "0 1 * * *"