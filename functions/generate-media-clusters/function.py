#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
import io
import os
import boto3
import json
from dateutil.relativedelta import relativedelta
from botocore.config import Config


# ## Utilitaires
def get_recency(start_date, end_date):
    try:
        months = (end_date.year - start_date.year) * 12 + (end_date.month - start_date.month)
        if months <= 3:
            return 'last_quarter'
        elif months <= 6:
            return 'last_semester'
        elif months <= 9:
            return 'last_three_quarters'        
        else:
            years = relativedelta(end_date,start_date).years
            if years <= 1:
                return 'last_year'
            elif years <= 2:
                return 'last_two_years'
            elif years <= 3:
                return 'last_three_years'
            elif years <= 4:
                return 'last_four_years'
            elif years <= 5:
                return 'last_five_years'
            else:
                return 'older'
    except Exception as e:
        print(e)


def handler(event, context):
    try:

        # Internal AWS resources
        aws_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        for key in aws_keys:
            with open('/{}/{}'.format(os.environ.get('SECRETS_FILE'), key), 'r') as s3_f:
                aws_keys[key] = s3_f.read()    

        aws_config = Config(region_name=aws_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=aws_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=aws_keys['AWS_SECRET_ACCESS_KEY']            
        )

        ssm = aws_session.client('ssm', config=aws_config)
        s3 = aws_session.client('s3', config=aws_config)
        s3_resource = aws_session.resource('s3')  

        pass_redshift = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        pass_reco = ssm.get_parameter(Name='DB_RECO_PASS', WithDecryption=True)
        bucket = 'big-data-media'
        
        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('DB_USER'), pass_redshift['Parameter']['Value'], os.environ.get('DB_HOST'), os.environ.get('DB_PORT'), os.environ.get('DB_DB')),pool_recycle=600)
        
        if event['data']['db_reco'] == 'dev':
            engine_reco = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('RECO_DEV_USER'), pass_reco['Parameter']['Value'], os.environ.get('RECO_DEV_HOST'), os.environ.get('RECO_DEV_PORT'), os.environ.get('RECO_DEV_DB')))
        elif event['data']['db_reco'] == 'prod':
            engine_reco = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('RECO_PROD_USER'), pass_reco['Parameter']['Value'], os.environ.get('RECO_PROD_HOST'), os.environ.get('RECO_PROD_PORT'), os.environ.get('RECO_PROD_DB')))

        providers = event['data']['providers']
        accepted_providers = [x for x,val in providers.items() if val == 1]

        recency_tags = [
            'last_quarter',
            'last_semester',
            'last_three_quarters',
            'last_year',
            'last_two_years',
            'last_three_years',
            'last_four_years',
            'last_five_years',
            'older'
        ]

        df = pd.read_sql_query("""SELECT mo.id, mo.type, mo.created, mo.provider, mo.live_from, mo.live_until, mp.contenttypestat FROM media_objects mo INNER JOIN media_programs mp ON mo.program_id = mp.id WHERE mo.live_from < CURRENT_DATE AND mo.live_until > DATE_ADD('day', 1, CURRENT_DATE) AND publish = '1' AND active = '1' AND mo.status = 'complete' and mo.type = 'video' AND mo.provider IN ('{}')""".format("','".join(accepted_providers)), parse_dates=['live_from','live_until'], con=engine)
        df = df[df['type']=='video']
        df['live_from'] = pd.to_datetime(df['live_from'], errors='coerce')
        df['live_until'] = pd.to_datetime(df['live_until'], errors='coerce')
        df['created'] = pd.to_datetime(df['created'], errors='coerce')
        df['provider_accepted'] = df['provider'].isin(accepted_providers)
        df['is_available'] = df['live_until'] > datetime.datetime.now()
        today = datetime.datetime.now()
        df['recency'] = df['live_from'].apply(lambda x: get_recency(x, today))
        df['id'] = df['id'].astype(int)
        df['id'] = df['id'].astype(str)

        clusters = {
            'default': 'all',
            'entertainment': 'last_year',
            'fiction': 'all',
            'information': 'individual',
            'knowledge': 'last_year',
            'lifestyle': 'last_year',
            'sport': 'last_year'
        }

        recency_clusters = {    
            'last_year': {'this_year': [
                'last_quarter',
                'last_semester',
                'last_three_quarters',
                'last_year'
            ],'older': [
                'last_two_years',
                'last_three_years',
                'last_four_years',
                'last_five_years',
                'older']},
            'all': {'': [
                'last_quarter',
                'last_semester',
                'last_three_quarters',
                'last_year',
                'last_two_years',
                'last_three_years',
                'last_four_years',
                'last_five_years',
                'older'
            ]},
            'individual': {
                'last_quarter': ['last_quarter'],
                'last_semester': ['last_semester'],
                'last_three_quarters': ['last_three_quarters'],
                'last_year': ['last_year'],
                'last_two_years': ['last_two_years'],
                'last_three_years': ['last_three_years'],
                'last_four_years': ['last_four_years'],
                'last_five_years': ['last_five_years'],
                'older': ['older']       
            }
        }        

        df['cluster'] = np.nan
        for key, value in clusters.items():
            for rec_key, rec in recency_clusters[value].items():
                df_update = df.loc[(df['contenttypestat'] == key) & (df['recency'].isin(rec))].copy()
                df_update['cluster'] = '{}{}'.format(key,'-{}'.format(rec_key) if rec_key != '' else rec_key)
                df.update(df_update)

        with engine_reco.begin() as conn:
            df[~df['cluster'].isna()][['id','type','created','cluster','provider','live_until']].to_sql(event['data']['table'], schema=event['data']['schema'], con=conn, chunksize=100000, method='multi', if_exists='replace', index=False, dtype={'id': sql.types.String,'type': sql.types.String,'created': sql.types.DateTime,'cluster': sql.types.String,'provider': sql.types.String,'live_until': sql.types.DateTime})                

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Sucessfully wrote on table {}'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y-%m-%d'), event['data']['table']))
        }

    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error writing table {} {}'.format(datetime.datetime.strftime(datetime.datetime.now(), '%y%m%d'), event['data']['table'], str(e)))
        }   