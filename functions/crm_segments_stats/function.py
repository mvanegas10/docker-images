#!/usr/bin/env python
# coding: utf-8

# ## Importations
import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
import boto3
import time
import os
import gzip
import io
import json
from botocore.config import Config
import sys


def handler(event, context):

    # ## Configuration
    try:
        # Internal AWS resources
        aws_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        for key in aws_keys:
            with open('/{}/{}'.format(os.environ.get('SECRETS_FILE'), key), 'r') as s3_f:
                aws_keys[key] = s3_f.read()        

        aws_session = boto3.session.Session(
            aws_access_key_id=aws_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=aws_keys['AWS_SECRET_ACCESS_KEY']            
        )

        s3 = aws_session.client('s3')
        s3_resource = aws_session.resource('s3')
        bucket = s3_resource.Bucket('big-data-media')

        colnamesdata = s3.get_object(Bucket=bucket.name, Key='crm/in/segments/segments_baseline/segments_labels.json')
        column_names = {key:value['actito_col'] for key, value in json.load(io.BytesIO(colnamesdata['Body'].read())).items() if value['is_actito'] == 1}

        for obj in bucket.objects.filter(Prefix='crm/out/actito/segments/'):
            if 'segments' in obj.key:
                data = s3_resource.Object(bucket.name, obj.key)
                with gzip.GzipFile(fileobj=data.get()['Body']) as gzfile:
                    df = pd.read_csv(io.BytesIO(gzfile.read()), encoding='utf-8-sig')

                    df = df.rename(columns=column_names)

                    df = df.replace(['last_7_days'],7)
                    df = df.replace(['last_30_days'],30)
                    df = df.replace(['last_90_days'],90)
                    df = df.replace(['last_180_days'],180)
                    df = df.replace(['last_365_days'],365)
                    df = df.replace(['older'],np.nan)

                    df = df[(~df['actito_id'].isna()) & (df['actito_id'] != 0)]

                    segments = [col for col in df.count().sort_values(ascending=False).index if col != 'actito_id']

                    recencies = [7,30,90,180,365]
                    not_accepted_recencies = recencies.copy()

                    for recency in recencies:
                        matrix = {}

                        not_accepted_recencies.remove(recency)

                        df_rec_dist = df.copy()

                        for rec in not_accepted_recencies:
                            df_rec_dist = df_rec_dist.replace([rec],np.nan)

                        counts = []

                        for segment in segments:
                            df_seg = df_rec_dist[~df_rec_dist[segment].isna()]
                            counts.append({'name': segment, 'value': int(df_rec_dist[segment].count())})
                            matrix[segment] = {}
                            for link in segments:
                                matrix[segment][link] = int(df_seg[link].count())
                                
                        df_rec_dist = df_rec_dist.set_index('actito_id')
                        df_rec_dist['segs'] = df_rec_dist.count(axis=1)
                        df_rec_dist = df_rec_dist.reset_index()
                        
                        res = {
                            'last_modified': data.last_modified.strftime('%Y/%m/%d'),
                            'count': len(df_rec_dist[df_rec_dist['segs'] > 0]),
                            'counts': list(counts),
                            'values': [list(row.values()) for row in matrix.values()],
                            'names': list(matrix.keys()),
                            'distribution': df_rec_dist[['segs','actito_id']].groupby(['segs']).count().reset_index().rename(columns={'actito_id':'counts'}).to_dict(orient='records')
                        }
                        
                        response = s3_resource.Object(bucket.name, 'crm/in/segments/stats/stats_{}.json'.format(recency)).put(Body=bytearray(json.dumps(res), 'utf8') )  

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Finished generating file {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), event['data']))
        }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        print(exc_type, exc_obj, exc_tb )

        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error generating file {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), '{}, {}, {}'.format(exc_type, fname, exc_tb.tb_lineno)))
        }