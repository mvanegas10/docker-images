kubeless function delete crm-segments-stats
kubeless function deploy crm-segments-stats --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env db_database=a360 \
                                --env db_host=10.200.0.84 \
                                --env db_port=5439 \
                                --env db_username=meiv \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --timeout 86400 \
                                --handler function.handler

kubeless trigger cronjob create \
  crm-segments-stats-daily \
  --function crm-segments-stats \
  --payload-from-file payload_weekly.json \
  --schedule "0 1 * * 0"