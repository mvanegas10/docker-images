# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
import dateutil
import boto3
import json
import os
import io
from io import BytesIO
from botocore.config import Config
import gzip
import shutil
import datetime
import hashlib


def upload_gzipped(bucket, key, fp, compressed_fp=None, content_type='text/plain'):
    fp.seek(0)
    compressed_fp = BytesIO()
    with gzip.GzipFile(fileobj=compressed_fp, mode='wb') as gz:
        shutil.copyfileobj(fp, gz)
    compressed_fp.seek(0)
    bucket.upload_fileobj(
        compressed_fp,
        key,
        {'ContentType': content_type, 'ContentEncoding': 'gzip'})


def handler(event, context):
    try:
        # Internal AWS resources
        aws_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        for key in aws_keys:
            with open('/aws-meiv/{}'.format(key), 'r') as s3_f:
                aws_keys[key] = s3_f.read()        

        aws_config = Config(region_name=aws_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=aws_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=aws_keys['AWS_SECRET_ACCESS_KEY']            
        )

        ssm = aws_session.client('ssm', config=aws_config)
        s3 = aws_session.client('s3', config=aws_config)
        s3_resource = aws_session.resource('s3')    

        # Recover parameters
        redshift_secret = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        profileid_salt = ssm.get_parameter(Name='RMB_PROFILEID_SALT', WithDecryption=True)
        adobe_access_key = ssm.get_parameter(Name='ADOBE_AWS_ACCESS_KEY', WithDecryption=True)
        adobe_secret_key = ssm.get_parameter(Name='ADOBE_AWS_SECRET_KEY', WithDecryption=True)

        
        # External resources
        aws_adobe_session = boto3.session.Session(
            aws_access_key_id=adobe_access_key['Parameter']['Value'],
            aws_secret_access_key=adobe_secret_key['Parameter']['Value']
        )

        s3_adobe_resource = aws_adobe_session.resource('s3')
        
        internal_bucket_name = os.environ.get('internal_bucket')
        internal_bucket = s3_resource.Bucket(internal_bucket_name)
        adobe_external_bucket = s3_adobe_resource.Bucket(os.environ.get('adobe_external_bucket'))
        oneplusx_external_bucket_name = os.environ.get('oneplusx_external_bucket')
        oneplusx_external_bucket = s3_resource.Bucket(oneplusx_external_bucket_name)

        today = datetime.datetime.now().day

        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('db_username'), redshift_secret['Parameter']['Value'], os.environ.get('db_host'), os.environ.get('db_port'), os.environ.get('db_database')))

        query = """SELECT 
                profile.uid, 
                profile.gender AS c_g_sexe, 
                profile.birthyear AS c_g_annee_de_naissance, 
                profile.zip AS c_g_code_postal, 
                profile.country,
                actito.assignedgender AS c_g_sexe_predit, 
                actito.assignedaddresspostalcode AS c_g_code_postal_attribue, 
                actito.assignedaddresscountry AS c_g_pays_attribue, 
                NULL AS c_g_annee_de_naissance_predit 
            FROM egos_gigya profile LEFT JOIN egos_actito actito ON profile.actitoid = actito.actitoid 
            """

        if today != 1:
            query += """WHERE (DATE_TRUNC('day',profile.created) = DATE_TRUNC('day',DATEADD('days', -2, CURRENT_DATE)) OR 
                    (DATE_TRUNC('day',profile.lastprofileupdateforadobe) = DATE_TRUNC('day',DATEADD('days', -2, CURRENT_DATE))))
                """

        df = pd.read_sql_query(query, engine)
        df['uid'] = df['uid'].apply(lambda x: hashlib.sha512('{}{}'.format(profileid_salt['Parameter']['Value'],x).encode("utf-8") ).hexdigest())
        df = df.set_index('uid')
        df['c_g_annee_de_naissance'] = df['c_g_annee_de_naissance'].apply(lambda x: str(int(x)) if x == x and x is not None else np.nan)
        df['c_g_annee_de_naissance_predit'] = df['c_g_annee_de_naissance_predit'].apply(lambda x: str(int(x)) if x == x and x is not None else np.nan)

        obj = df.to_dict('index')

        adobe_data_str = '\n'.join(['{}\t{}'.format(key, ','.join(['"{}"="{}"'.format(col, data) for col, data in value.items() if data == data and data is not None and data != ''])) for key, value in obj.items()])

        today = datetime.datetime.now()
        today_utc = datetime.datetime.now(datetime.timezone.utc)
        first_date = datetime.datetime(1970,1,1,tzinfo=datetime.timezone.utc)

        adobe_file_name = '{}{}_{}.{}.gz'.format(
            event['data']['adobe_file_name'], 
            event['data']['daily_name'] if today != 1 else event['data']['monthly_name'],
            int((today_utc-first_date).total_seconds()), 
            event['data']['daily_extension'] if today != 1 else event['data']['monthly_extension']
        )

        adobe_internal_file = '{}/adobe/{}/{}'.format(os.environ.get('internal_s3_uri'), today.strftime('%Y/%m/%d'), adobe_file_name)
        adobe_external_file = '{}/{}'.format(os.environ.get('adobe_s3_uri'), adobe_file_name)

        upload_gzipped(internal_bucket, adobe_internal_file, BytesIO(bytearray(adobe_data_str, 'utf8')))
        upload_gzipped(adobe_external_bucket, adobe_external_file, BytesIO(bytearray(adobe_data_str, 'utf8')))

        oneplusx_file_name = '{}_{}.csv'.format(
            today.strftime('%Y%m%d%H%M%S'), 
            os.environ.get('oneplusx_base_name'))

        oneplusx_internal_file = '{}/1plusx/{}/{}'.format(os.environ.get('internal_s3_uri'), today.strftime('%Y/%m/%d'), oneplusx_file_name)

        oneplusx_external_file = '{}'.format(oneplusx_file_name)

        csv_buffer_internal = io.StringIO()
        df.reset_index().to_csv(csv_buffer_internal, index=False, encoding='utf-8-sig')
        response = s3_resource.Object(internal_bucket_name, oneplusx_internal_file).put(Body=csv_buffer_internal.getvalue())

        s3_resource.meta.client.copy({
                'Bucket': internal_bucket_name,
                'Key': oneplusx_internal_file
            }, oneplusx_external_bucket_name, 
            oneplusx_external_file,
            {'ACL': 'bucket-owner-full-control'}
        )      

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Finished {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), event['data']))
        }    

    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error generating the dumps {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), e))
        }