kubeless -n dumps function delete dump-gigya-rmb
kubeless -n dumps function deploy dump-gigya-rmb --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env db_database=a360 \
                                --env db_host=10.200.0.84 \
                                --env db_port=5439 \
                                --env db_username=meiv \
                                --env internal_bucket=big-data-media  \
                                --env internal_s3_uri=out/rmb \
                                --env adobe_external_bucket=demdex-s2s-clients \
                                --env adobe_s3_uri=rmb \
                                --env oneplusx_external_bucket=1plusx-rmb-data-transfer \
                                --env oneplusx_base_name=1plusx \
                                --secrets aws-meiv \
                                --timeout 3600 \
                                --handler function.handler

kubeless -n dumps trigger cronjob create \
  dump-gigya-rmb-daily \
  --function dump-gigya-rmb \
  --payload-from-file payload_daily.json \
  --schedule "0 3 * * *"


kubeless -n dumps trigger cronjob create \
  dump-gigya-rmb-monthly \
  --function dump-gigya-rmb \
  --payload-from-file payload_monthly.json \
  --schedule "0 3 1 * *"
