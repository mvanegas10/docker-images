kubeless function delete crm-segments-mapping
kubeless function deploy crm-segments-mapping --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env db_database=a360 \
                                --env db_host=10.200.0.84 \
                                --env db_port=5439 \
                                --env db_username=meiv \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --timeout 86400 \
                                --handler function.handler

kubeless trigger cronjob create \
  crm-segments-mapping-daily \
  --function crm-segments-mapping \
  --payload-from-file payload_daily.json \
  --schedule "0 1 * * *"