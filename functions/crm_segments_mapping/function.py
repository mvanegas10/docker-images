#!/usr/bin/env python
# coding: utf-8

# ## Importations
import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
import boto3
import time
import os
import io
import json
from botocore.config import Config
import sys


def send_email(ses_client, subject, body, dest_addresses):
    CHARSET = 'UTF-8'
    body_html = """<html><head></head><body><h1>{}</h1><p>{}</p></body></html>""".format(subject, body)
    body_text = ('{}\r\n{}'.format(subject, body))

    response = ses_client.send_email(
        Destination={
            'ToAddresses': [f'{x}@rtbf.be' for x in dest_addresses],
        },
        Message={
            'Body': {
                'Html': {
                    'Charset': CHARSET,
                    'Data': body_html,
                },
                'Text': {
                    'Charset': CHARSET,
                    'Data': body_text,
                },
            },
            'Subject': {
                'Charset': CHARSET,
                'Data': subject,
            },
        },
        Source='Gestion Donnees <gestion-donnees@rtbf.be>'
    )


def handler(event, context):

    # ## Configuration
    try:
        # Internal AWS resources
        aws_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        for key in aws_keys:
            with open('/{}/{}'.format(os.environ.get('SECRETS_FILE'), key), 'r') as s3_f:
                aws_keys[key] = s3_f.read()        

        aws_config = Config(region_name=aws_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=aws_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=aws_keys['AWS_SECRET_ACCESS_KEY']            
        )

        ssm = aws_session.client('ssm', config=aws_config)
        s3 = aws_session.client('s3', config=aws_config)
        s3_resource = aws_session.resource('s3')
        ses = aws_session.client('ses', config=aws_config)     
        redshift = aws_session.client('redshift-data', config=aws_config) 

        # Redshift engine
        redshift_secret = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('db_username'), redshift_secret['Parameter']['Value'], os.environ.get('db_host'), os.environ.get('db_port'), os.environ.get('db_database')))

        bucket = event['data']['bucket']

        # Get the table mappings baseline
        table_mappings = json.load(io.BytesIO(s3.get_object(Bucket=bucket, Key=event['data']['table_mappings_file'])['Body'].read()))

        df_segment_mappings = pd.DataFrame(columns=['id','table','segment'])

        # Get all the segments-content mappings
        df_seg_baseline = pd.read_sql_query("""SELECT * FROM crm_segments_baseline""", engine)
        df_seg_baseline['processed'] = np.nan
        df_seg_baseline['error'] = np.nan

        # For each mapping, get all the media id's 
        for i, row in df_seg_baseline.iterrows():
            query = f"""
                SELECT {table_mappings[row['egos_table']]['id']} as id, 
                    '{table_mappings[row['egos_table']]['table_label']}' AS table, 
                    '{row['segment']}' AS segment, 
                    {"'{}'".format(table_mappings[row['egos_table']]['type']) 
                        if 'type' in table_mappings[row['egos_table']] 
                        else table_mappings[row['egos_table']]['type_col']
                    } AS type 
                FROM {table_mappings[row['egos_table']]['table']}
                WHERE {row['dimension']} = '{row['variable']}'"""

            try:
                df = pd.read_sql_query(query, engine)
                df_seg_baseline.at[i, 'processed'] = len(df)
                if len(df) == 0:
                    df_seg_baseline.at[i, 'error'] = 'No media found for query {}'.format(query)
                    print('Empty data frame\n{}\n{}'.format(query,''.join('-'*100)))
                else:
                    df_segment_mappings = df_segment_mappings.append(df, ignore_index=True)

            except Exception as e:
                print('Exception\n{}\n{}\n{}'.format(str(e).split('\n')[0],query,''.join('-'*100)))
                
        # Write the errors log file
        csv_buffer_input = io.StringIO()
        df_seg_baseline = df_seg_baseline.sort_values('processed')
        df_seg_baseline.to_csv(csv_buffer_input, index=False, encoding='utf-8-sig')
        response = s3_resource.Object(bucket, '{}/{}'.format(event['data']['processed_log'], datetime.datetime.now().strftime('%Y/%m/%d'))).put(Body=csv_buffer_input.getvalue())

        df_segment_mappings = df_segment_mappings.rename(columns={'type':'media_type'})
        df_segment_mappings['segment'] = df_segment_mappings['segment'].str.replace('\xa0','')

        # Write the id mapping table
        with engine.begin() as conn:
            df_segment_mappings.to_sql(event['data']['table_segments_mapping'], schema='public', con=conn, chunksize=100000, method='multi', if_exists='replace', index=False, dtype={'id': sql.types.String,'table': sql.types.String, 'segment': sql.types.String, 'media_type': sql.types.String})


        today = datetime.datetime.now()
        
        with engine.connect() as conn:
            result = conn.execute("""
                UNLOAD ( $$
                WITH q1 AS (
                SELECT uid, MD5(CONCAT(media.mediaid,mediatype.mediatype)) AS hashmediatype, MAX(dates.date) AS max_date
                    FROM datawarehouse.egos_fct_trackers tracker
                    INNER JOIN datawarehouse.egos_tracker_dim_date dates ON tracker.fk_date = dates.pk_date
                    INNER JOIN datawarehouse.egos_tracker_dim_users users ON tracker.fk_uid = users.pk_uid
                    INNER JOIN datawarehouse.egos_tracker_dim_media media ON tracker.fk_media = media.pk_media
                    INNER JOIN datawarehouse.egos_tracker_dim_mediatype mediatype ON tracker.fk_mediatype = mediatype.pk_mediatype
                WHERE octet_length(uid) < 51
                and octet_length(uid) != 36
                and uid != 'Private'
                and uid != 'anonymous'
                GROUP BY uid,hashmediatype),
                q2 AS (
                SELECT MD5(CONCAT(id,media_type)) AS hashmediatype, segment FROM crm_segments_mapping)
                SELECT actitoid, segment, MAX(max) AS last_date, CASE WHEN last_date >= date_add('days',-7,CURRENT_DATE) THEN 'last_7_days'
                    WHEN last_date >= date_add('days',-30,CURRENT_DATE) THEN 'last_30_days'
                    WHEN last_date >= date_add('days',-90,CURRENT_DATE) THEN 'last_90_days'
                    WHEN last_date >= date_add('days',-180,CURRENT_DATE) THEN 'last_180_days'
                    WHEN last_date >= date_add('days',-365,CURRENT_DATE) THEN 'last_365_days'
                    ELSE 'older'
                    END AS recency
                FROM 
                (SELECT *
                FROM (
                    (SELECT actitoid, segment, MAX(max_date) AS max
                    FROM q1 INNER JOIN q2 ON q1.hashmediatype = q2.hashmediatype INNER JOIN egos_gigya gigya ON q1.uid = gigya.uid
                    GROUP BY actitoid, segment)
                    UNION ALL
                    (SELECT actitoid, segment, created AS max
                    FROM egos_gigya gigya INNER JOIN egos_regsource_segment regsource ON gigya.regsource = regsource.regsource)
                )
                )
                GROUP BY actitoid, segment
                $$)
                TO 's3://big-data-media/crm/user_segments/{}/{}/{}/segments_baseline' iam_role 'arn:aws:iam::435994096874:role/myRedshiftRole'
                    PARALLEL ON
                    ALLOWOVERWRITE
                    PARQUET 
                """.format(today.strftime('%Y'), today.strftime('%m'), today.strftime('%d')))
            print('UNLOAD')
        
            try:
                conn.execute("""TRUNCATE table temp_egos_user_segments""")
            except Exception as e:
                conn.execute("""create table temp_egos_user_segments (
                    actitoid  integer encode zstd,
                    segment   varchar(100),
                    last_date timestamp encode az64,
                    recency   varchar(50)
                )""")          
            print('TRUNCATE')

            conn.execute("""COPY temp_egos_user_segments
                    FROM 's3://big-data-media/crm/user_segments/{}/{}/{}/'
                    IAM_ROLE 'arn:aws:iam::435994096874:role/myRedshiftRole'
                    FORMAT AS PARQUET
                    MAXERROR AS 0""".format(today.strftime('%Y'), today.strftime('%m'), today.strftime('%d')))
            print('COPY')

            result = conn.execute("""select count(*) from egos_user_segments union all select count(*) FROM  temp_egos_user_segments""")
            values = []

            for row in result:
                values.append(row[0])

            print(f'Comparaison : egos_user_segments (d-1) ({values[0]}) vs egos_user_segments (d-1) ({values[1]}). Resultat : {int((values[1]/values[0])*100)}%')

            if values[1] > (values[0]*event['data']['limit_delete_table']):
                response = redshift.execute_statement(
                    Database='a360',
                    ClusterIdentifier='cluster-mktstr',
                    DbUser=os.environ['db_username'],
                    Sql='TRUNCATE table egos_user_segments'
                )

                res = ''

                while res != 'FINISHED':
                    response = redshift.describe_statement(
                        Id=response['Id']
                    )
                    res = response['Status']
                    time.sleep(5)

                                
                conn.execute("""COPY egos_user_segments
                        FROM 's3://big-data-media/crm/user_segments/{}/{}/{}/'
                        IAM_ROLE 'arn:aws:iam::435994096874:role/myRedshiftRole'
                        FORMAT AS PARQUET
                        MAXERROR AS 0""".format(today.strftime('%Y'), today.strftime('%m'), today.strftime('%d')))
                
                result = conn.execute("""select count(*) from egos_user_segments""")
                counts = []
                for row in result:
                    counts.append(row[0])
                if sum(counts) < (values[0]*event['data']['limit_delete_table']):
                    subject = 'Warning: CRM Affinity segments daily process'
                    body = f"""Today's CRM segments generation execution finished up with an unusual result.<br/>
                        The egos_user_segments table generated today contains {sum(counts)} records. This is {int(sum(counts)/values[0])}% of the previeus table.<br/>
                        Please refer to the <a href='https://rancher-ew1.bda-connect.be/p/local:p-tsvkk/workload/cronjob:default:trigger-crm-segments-mapping' 
                        target='_blank'>function's trigger</a> at Rancher to check the details.</br>Best,</br>#gestiondonnees"""
                    send_email(ses, subject, body, event['data']['destination_addresses'])
                    print('SEND MAIL!!')                    

                print('COPY')

            else:
                subject = 'Warning: CRM Affinity segments daily process'
                body = f"""Today's CRM segments generation execution finished up with an unusual result.<br/>
                    The egos_user_segments table generated today contained less than {event['data']['limit_delete_table']*100}% of records from the previous one.<br/>
                    Please refer to the <a href='https://rancher-ew1.bda-connect.be/p/local:p-tsvkk/workload/cronjob:default:trigger-crm-segments-mapping' 
                    target='_blank'>function's trigger</a> at Rancher to check the details.</br>Best,</br>#gestiondonnees"""
                send_email(ses, subject, body, event['data']['destination_addresses'])
                print('SEND MAIL!!')

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Finished generating egos_user_segments {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), event['data']))
        }    

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        print(exc_type, exc_obj, exc_tb)
        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error egos_user_segments {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), '{}, {}, {}'.format(exc_type, fname, exc_tb.tb_lineno)))
        }