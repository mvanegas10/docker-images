kubeless -n monitoring-and-tools function delete reco-pipeline-daily-figures
kubeless -n monitoring-and-tools function deploy reco-pipeline-daily-figures --runtime python3.8 \
                                --from-file function.py \
                                --env DB_DB=a360 \
                                --env DB_HOST=10.200.0.84 \
                                --env DB_PORT=5439 \
                                --env DB_USER=meiv \
                                --env RECO_DEV_DB=bdd_reco \
                                --env RECO_DEV_HOST=database-1.cluster-cmewnkhytu1o.eu-central-1.rds.amazonaws.com \
                                --env RECO_DEV_PORT=5432 \
                                --env RECO_DEV_USER=postgres \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --dependencies requirements.txt \
                                --timeout 3600 \
                                --handler function.handler

kubeless -n monitoring-and-tools trigger cronjob create \
  reco-pipeline-daily-figures-daily \
  --function reco-pipeline-daily-figures \
  --payload-from-file payload_daily.json \
  --schedule "0 6 * * *"