#!/usr/bin/env python
# coding: utf-8

# ## Importations
import pandas as pd
import numpy as np
import sqlalchemy as sql
import configparser
import datetime
import io
import json
from botocore.config import Config
import boto3
import sys
import os


def handler(event, context):

    try:
        # Configuration

        secret_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        providers = {'AREMA': True,
            'ARTE': False,
            'CHAPTERAUTO': True,
            'CHAPTERCMS': True,
            'CRYO': True,
            'DAILYMOTION': False,
            'DALET': True,
            'FTP': False,
            'INGESTVOD': False,
            'LIVECENTER': True,
            'NETIA': True,
            'OTHER': False,
            'PARTNERAB': False,
            'SONUMA': True,
            'VIMEO': False,
            'WHATSON': True,
            'YOUTUBE': False}        

        for key in secret_keys:
            with open('/{}/{}'.format(os.environ.get('SECRETS_FILE'), key), 'r') as s3_f:
                secret_keys[key] = s3_f.read()        

        aws_config = Config(region_name=secret_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=secret_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=secret_keys['AWS_SECRET_ACCESS_KEY']            
        )
        
        ssm = aws_session.client('ssm', config=aws_config)

        pass_redshift = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        pass_reco = ssm.get_parameter(Name='DB_RECO_PASS', WithDecryption=True)
        
        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('DB_USER'), pass_redshift['Parameter']['Value'], os.environ.get('DB_HOST'), os.environ.get('DB_PORT'), os.environ.get('DB_DB')),pool_recycle=600)
        
        engine_dev = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('RECO_DEV_USER'), pass_reco['Parameter']['Value'], os.environ.get('RECO_DEV_HOST'), os.environ.get('RECO_DEV_PORT'), os.environ.get('RECO_DEV_DB')))

        # Constants 

        today = datetime.datetime.combine(datetime.date.today(), datetime.datetime.min.time())

        df_counts = pd.DataFrame(columns=['id','type'])

        df = pd.read_sql_query("""SELECT mo.id, provider FROM media_objects mo INNER JOIN media_programs mp ON mo.program_id = mp.id
            WHERE mo.live_from < CURRENT_DATE AND mo.live_until > DATE_ADD('day', 1, CURRENT_DATE) AND publish = '1' AND active = '1' AND mo.status = 'complete' and mo.type = 'video'
        """, engine)
        df['id'] = df['id'].astype(int)

        df_counts = df[['id']].copy()
        df_counts['available_auvio'] = 1

        df_post = pd.read_sql_query("""SELECT id, recommendable FROM rtbfv2.media WHERE type = 'video'""", engine_dev)
        df_post['id'] = df_post['id'].astype(int)

        df_post_rec = df.merge(df_post)

        df_temp = df_post_rec[['id']]
        df_temp['db_reco'] = 1
        df_counts = df_counts.merge(df_temp, how='left')

        df_post_rec = df_post_rec[df_post_rec['recommendable'] == True]

        df_temp = df_post_rec[['id']]
        df_temp['recommendable'] = 1
        df_counts = df_counts.merge(df_temp, how='left')

        acc_prov = [key for key in providers.keys() if providers[key]]

        df_post_rec = df_post_rec[df_post_rec['provider'].isin(acc_prov)]

        df_temp = df_post_rec[['id']]
        df_temp['accepted_provider'] = 1
        df_counts = df_counts.merge(df_temp, how='left')

        df_feats = pd.read_sql_query("""SELECT id, jsonb_array_length(topics::JSONB) as topics, jsonb_array_length(entities::JSONB) as entities, jsonb_array_length(coarse_topics::JSONB) as coarse_topics FROM rtbfv2.media_features WHERE type = 'video'""", engine_dev)
        df_feats['id'] = df_feats['id'].astype(int)
        df_feats = df_feats.sort_values(by=['id','entities'], ascending=False).drop_duplicates('id', keep='first')

        df_post_rec = df_post_rec.merge(df_feats)

        df_temp = df_post_rec[['id']]
        df_temp['topics_entities'] = 1
        df_counts = df_counts.merge(df_temp, how='left')

        df_post_rec = df_post_rec[(df_post_rec['entities'] > 0) | (df_post_rec['coarse_topics'] > 0) | (df_post_rec['topics'] > 0)]

        df_temp = df_post_rec[['id']]
        df_temp['topics_entities_not_empty'] = 1
        df_counts = df_counts.merge(df_temp, how='left')

        df_reco = pd.read_sql_query("""select media_id as id, jsonb_object_keys(REPLACE(reco,'''','"')::JSONB) AS recos from rtbfv2.recommendation""", engine_dev)
        df_reco['id'] = df_reco['id'].astype(int)
        df_reco = df_reco.groupby('id').count().reset_index()
        df_reco = df_reco[df_reco['recos'] > 1]

        df_post_rec = df_post_rec.merge(df_reco)

        df_temp = df_post_rec[['id']]
        df_temp['more_than_one_reco'] = 1
        df_counts = df_counts.merge(df_temp, how='left')

        df_reco = df_reco[df_reco['recos'] > 15]
        df_post_rec = df_post_rec.merge(df_reco)   

        df_temp = df_post_rec[['id']]
        df_temp['more_than_fifteen_reco'] = 1
        df_counts = df_counts.merge(df_temp, how='left')             

        df_counts['last_update'] = today
        df_counts['type'] = 'video'

        with engine_dev.begin() as conn:
            df_counts.to_sql(event['data']['table'], schema=event['data']['schema'], con=conn, method='multi', if_exists='replace', index=False, dtype={
                'id': sql.types.Integer,
                'type': sql.types.String,
                'available_auvio': sql.types.Integer,
                'db_reco': sql.types.Integer,
                'recommendable': sql.types.Integer,
                'accepted_provider': sql.types.Integer,
                'topics_entities': sql.types.Integer,
                'topics_entities_not_empty': sql.types.Integer,
                'more_than_one_reco': sql.types.Integer,
                'more_than_fifteen_reco': sql.types.Integer,
                'last_update': sql.types.DateTime
            })  

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Finished function excecution {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), event['data']))
        }    

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error in function {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), '{}, {}, {}'.format(exc_type, fname, exc_tb.tb_lineno)))
        }    