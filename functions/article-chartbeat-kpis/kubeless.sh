kubeless function delete article-chartbeat-kpis
kubeless function deploy article-chartbeat-kpis --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env db_database=a360 \
                                --env db_host=10.200.0.84 \
                                --env db_port=5439 \
                                --env db_username=meiv \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --timeout 432000 \
                                --handler function.handler

kubeless trigger cronjob create \
  article-chartbeat-kpis-daily \
  --function article-chartbeat-kpis \
  --schedule "3 * * * *"                                