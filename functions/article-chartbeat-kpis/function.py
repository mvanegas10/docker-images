#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
import requests
import boto3
import json
import os
import re
import io
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import configparser
import multiprocessing
import hashlib
from botocore.config import Config


# ## Utilitaires
def get_article_id(path):
    if 'rtbf.be/auvio/' in path:
        return np.nan
    match = re.search('\?id=[0-9]+($|&)', path)
    if match is not None:
        articleid = path[match.start()+4: match.end()]
        if articleid[-1] == '&':
            return articleid[:-1]
        else:
            return articleid
    else:
        np.nan


def get_query_id(**params):
    response = requests.get('https://api.chartbeat.com/query/v2/submit/page/?apikey={}&endpoint={}&host={}&email={}&user_id={}&limit={}&start={}&end={}&tz={}&dimensions={}&metrics={}&{}={}'.format(params['api_key'], params['endpoint'], params['host'], params['email'], params['user_id'], params['limit'], params['start_date'], params['end_date'], params['tz'], params['dimensions'], params['metrics'], params['filter_key'], params['filter_value'])).json()
    return response['query_id'] if 'query_id' in response else 'Error'


def get_query_status(query_id, **config):
    response = requests.get('https://api.chartbeat.com/query/v2/status/?apikey={}&host={}&query_id={}'.format(config['api_key'], config['host'], query_id)).json()
    return response['status'] if 'status' in response else 'Error'


def ask_for_data(query_id, max_requests=10, **config):
    status = get_query_status(query_id, **config)
    if (status in ['completed', 'downloaded']):
        return status
    elif max_requests > 0:
        time.sleep(10)
        status = ask_for_data(query_id, max_requests-1, **config)
    else:
        return 'Service down'


def get_data(query_id, df_metadata, **config):
    response = requests.get('https://api.chartbeat.com/query/v2/fetch/?apikey={}&host={}&query_id={}'.format(config['api_key'], config['host'], query_id)).text
    df = pd.read_csv(io.StringIO(response))

    if len(df) >= config['limit']:
        print(date_str)

    df['articleid'] = df['path'].apply(get_article_id)
    df = df[(~df['articleid'].isna()) & ~(df['articleid'].str.len() == 0)]

    df = df.rename(columns={'tz_time':'datetime','page_total_time':'total_time'})

    df['len'] = df['path'].str.len()
    df = df.sort_values(['datetime','articleid','len'])

    df = df.groupby(['datetime','articleid']).agg({'total_time': 'sum', 'page_views': 'sum', 'path': 'first'}).reset_index()

    df = df.merge(df_metadata, left_on='articleid', right_on='id', how='left')

    df['articleid'] = df['articleid'].astype(int)
    df['page_views'] = df['page_views'].astype(int)
    df['total_time'] = df['total_time'].astype(int)
    df['nb_words'] = df['nb_words'].apply(lambda x: int(x) if x == x else 0)    

    return df[['datetime', 'articleid', 'nb_words', 'total_time', 'page_views', 'path']]


def handler(event, context):
    try:

        # Internal AWS resources
        aws_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        for key in aws_keys:
            with open('/{}/{}'.format(os.environ.get('SECRETS_FILE'), key), 'r') as s3_f:
                aws_keys[key] = s3_f.read()    

        aws_config = Config(region_name=aws_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=aws_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=aws_keys['AWS_SECRET_ACCESS_KEY']            
        )

        ssm = aws_session.client('ssm', config=aws_config)
        s3 = aws_session.client('s3', config=aws_config)
        s3_resource = aws_session.resource('s3') 

        bucket = 'big-data-media' 

        parameter = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        chartbeat_api_key = ssm.get_parameter(Name='MEIV_CHARTBEAT_API_KEY', WithDecryption=True)
        
        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('db_username'), parameter['Parameter']['Value'], os.environ.get('db_host'), os.environ.get('db_port'), os.environ.get('db_database')),pool_recycle=600)
        
        yesterday = datetime.datetime.strptime(event['data']['day'], '%Y-%m-%d') if 'day' in event['data'] else (datetime.datetime.now().date() - datetime.timedelta(days=1))
        print(yesterday)
        yesterday_str = yesterday.strftime('%Y-%m-%d')

        config = event['data']['query']['config']
        config.update({'api_key': chartbeat_api_key['Parameter']['Value']})

        query_params = {
            **config,
            **event['data']['query']['params'],
            'start_date': yesterday_str, 
            'end_date': yesterday_str
        }

        query_id = get_query_id(**query_params)

        df_metadata = pd.read_sql_query("""SELECT CAST(id AS varchar), MAX(charcounter) AS nb_words
            FROM bda_spectrum_poc.parquet t INNER JOIN article_article art
                ON t.data.context.screen.media.id = art.id
            WHERE
                t.partition_0 = '{}'
                AND t.partition_1 = '{}'
                AND t.partition_2 = '{}'
                AND t.data.context.screen.media.type = 'article'
            GROUP BY id""".format(yesterday.strftime('%Y'), yesterday.strftime('%m'), yesterday.strftime('%d')), engine)

        if query_id != 'Error':
            status = ask_for_data(query_id, **config)
            if status != 'Service down':
                df = get_data(query_id, df_metadata, **query_params)

                try:
                    csv_buffer = io.StringIO()
                    df.to_csv(csv_buffer, index=False, encoding='utf-8-sig', date_format='%Y-%m-%d %H:%M:%S', na_rep='0')
                    response = s3_resource.Object(bucket, 'playground/chartbeat/kpi/{}_article_metrics.csv'.format(yesterday.strftime('%Y/%m/%d/%Y%m%d'))).put(Body=csv_buffer.getvalue())

                    return {
                        'statusCode': response['ResponseMetadata']['HTTPStatusCode'],
                        'body': json.dumps('{} : {} _article_metrics.csv'.format(datetime.datetime.strftime(yesterday, '%y%m%d'), 'Saved file' if response['ResponseMetadata']['HTTPStatusCode'] == 200 else 'HTTP Response. Could not write file {} articles_media.csv'.format(datetime.datetime.strftime(yesterday, '%y%m%d'))))
                    }
                except Exception as e:
                    return {
                        'statusCode': 500,
                        'body': json.dumps('{} : Could not write file _article_metrics.csv {}'.format(datetime.datetime.strftime(yesterday, '%y%m%d'), str(e)))
                    }                  

            else:
                return {
                    'statusCode': 500,
                    'body': json.dumps('{} : Could not write file _article_metrics.csv. Chartbeat status {}'.format(datetime.datetime.strftime(yesterday, '%y%m%d'), status))
                }       
        else:
            return {
                'statusCode': 500,
                'body': json.dumps('{} : Could not write file _article_metrics.csv. Chartbeat query {}'.format(datetime.datetime.strftime(yesterday, '%y%m%d'), query_id))
            }
    except Exception as e:
        return {
            'statusCode': 500,
            'body': json.dumps('{} : Could not write file _article_metrics.csv {}'.format(datetime.datetime.now().strftime('%y%m%d'), str(e)))
        }              