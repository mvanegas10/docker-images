#!/usr/bin/env python
# coding: utf-8

# ## Importations
import pandas as pd
import numpy as np
import requests
import io
import os
import sys
import boto3
import json
import datetime
from botocore.config import Config
import string
from webdav3.client import Client



def handler(event, context):

    try:
        # Configuration

        secret_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': '',
            'WEBDAV_PASS': ''
        }

        for key in secret_keys:
            with open('/{}/{}'.format(os.environ.get('SECRETS_FILE'), key), 'r') as s3_f:
                secret_keys[key] = s3_f.read()        

        aws_config = Config(region_name=secret_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=secret_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=secret_keys['AWS_SECRET_ACCESS_KEY']            
        )

        options_webdav = {
            'webdav_hostname': event['data']['webdavhost'],
            'webdav_login': event['data']['webdavuser'],
            'webdav_password': secret_keys['WEBDAV_PASS']
        }        

        # Constants 

        CHARSET = 'UTF-8'

        webdav = Client(options_webdav)
        ses = aws_session.client('ses', config=aws_config)
        s3 = aws_session.client('s3', config=aws_config)
        s3_resource = aws_session.resource('s3')    
        bucket = event['data']['bucket']        

        today = datetime.datetime.combine(datetime.date.today(), datetime.datetime.min.time()) - datetime.timedelta(days=1)
        variables = {
            'cas': 'Cas',
            'deces': 'Décès',
            'hospitalisations': 'Hospitalisations',
            'tests': 'Tests',
            'vaccine_1dose': 'Vaccinés (1er dose)',
            'taux_positivite': 'Taux de positivité'  
        }        

        # CAS : CASES 
        df_cas = pd.read_excel('https://epistat.sciensano.be/Data/COVID19BE.xlsx', sheet_name='CASES_AGESEX')
        df_cas['DATE'] = df_cas['DATE'].fillna(today)
        df_cas = df_cas.groupby('DATE').sum().reset_index()
        df_cas = df_cas.rename(columns={'DATE':'date','CASES':'aug_cas'})
        df_cas = df_cas[['date','aug_cas']]

        # Hospitalisations : NEW_IN 
        df_hosp = pd.read_excel('https://epistat.sciensano.be/Data/COVID19BE.xlsx', sheet_name='HOSP')
        df_hosp['DATE'] = df_hosp['DATE'].fillna(today)
        df_hosp = df_hosp.groupby('DATE').sum().reset_index()
        df_hosp = df_hosp.rename(columns={'DATE':'date','NEW_IN':'aug_hospitalisations'})
        df_hosp = df_hosp[['date','aug_hospitalisations']]
        df_hosp.tail()

        # Décès : DEATHS 
        df_dec = pd.read_excel('https://epistat.sciensano.be/Data/COVID19BE.xlsx', sheet_name='MORT')
        df_dec['DATE'] = df_dec['DATE'].fillna(today)
        df_dec = df_dec.groupby('DATE').sum().reset_index()
        df_dec = df_dec.rename(columns={'DATE':'date','DEATHS':'aug_deces'})
        df_dec = df_dec[['date','aug_deces']]

        # Tests

        df_tests = pd.read_excel('https://epistat.sciensano.be/Data/COVID19BE.xlsx', sheet_name='TESTS')
        df_tests['DATE'] = df_tests['DATE'].fillna(today)
        df_tests = df_tests.groupby('DATE').sum().reset_index()
        df_tests = df_tests.rename(columns={'DATE':'date','TESTS_ALL':'aug_tests','TESTS_ALL_POS':'taux_positivite'})
        df_tests = df_tests[['date','aug_tests', 'taux_positivite']]

        # Vaccinations

        df_vaccs = pd.read_excel('https://epistat.sciensano.be/Data/COVID19BE.xlsx', sheet_name='VACC')
        df_vaccs = df_vaccs[df_vaccs['DOSE'] == 'A']
        df_vaccs['DATE'] = df_vaccs['DATE'].fillna(today)
        df_vaccs = df_vaccs.groupby('DATE').sum().reset_index()
        df_vaccs = df_vaccs.rename(columns={'DATE':'date','COUNT':'aug_vaccine_1dose'})
        df_vaccs = df_vaccs[['date','aug_vaccine_1dose']]

        # Calculate trends

        min_date = np.min([
            df_cas['date'].min(),
            df_dec['date'].min(),
            df_hosp['date'].min(),
            df_tests['date'].min(),
            df_vaccs['date'].min()
        ])

        max_date = np.max([
            df_cas['date'].max(),
            df_dec['date'].max(),
            df_hosp['date'].max(),
            df_tests['date'].max(),
            df_vaccs['date'].max()
        ])

        df = pd.DataFrame({'date': np.array(pd.date_range(start=min_date, end=max_date, freq='D'))})
        df = df.merge(df_cas, how='left')
        df = df.merge(df_dec, how='left')
        df = df.merge(df_hosp, how='left')
        df = df.merge(df_tests, how='left')
        df = df.merge(df_vaccs, how='left')

        for var in variables.keys():
            base_moy = 'aug_{}'.format(var) if var != 'taux_positivite' else var
            roundval = '{:.0%}' if var != 'taux_positivite' else '{:.1%}'
            shiftval = 3 if var != 'hospitalisations' else 0
            
            total = 'total_{}'.format(var)
            
            moy7 = '{}_moy_7J'.format(var)
            moy14 = '{}_moy_14J'.format(var)
            

            deltamoy7 = 'perc_diff_{}_moy_7J'.format(var)
            deltamoy14 = 'perc_diff_{}_moy_14J'.format(var)    
            
            df[base_moy] = df[base_moy].fillna(0)
            df[total] = df[base_moy].cumsum().astype(int)
            
            df[moy7] = df[base_moy].rolling(window=7).mean().shift(shiftval)
            df[moy14] = df[base_moy].rolling(window=14).mean().shift(shiftval)
            df[deltamoy7] = df[moy7].pct_change(7)
            df[deltamoy14] = df[moy14].pct_change(14)    

            if var == 'taux_positivite':
                df[moy7] = df[moy7]/df['tests_moy_7J']
                df[moy14] = df[moy14]/df['tests_moy_14J']
                df[deltamoy7] = df[moy7] - df[moy7].shift(7)
                df[deltamoy14] = df[moy14] - df[moy14].shift(14)        
                
            df[moy7] = df[moy7].fillna(0).replace(np.inf,0)
            df[moy14] = df[moy14].fillna(0).replace(np.inf,0)
            df[deltamoy7] = df[deltamoy7].fillna(0).replace(np.inf,0)
            df[deltamoy14] = df[deltamoy14].fillna(0).replace(np.inf,0)        
            
        # Write results

        file_name = event['data']['prefix_output']

        letters = list(string.ascii_uppercase) + ['A{}'.format(l) for l in list(string.ascii_uppercase)]

        csv_buffer_internal = io.BytesIO()
        writer = pd.ExcelWriter(csv_buffer_internal, engine='xlsxwriter')

        workbook  = writer.book
        format_number = workbook.add_format({'num_format': '#,##0'})
        format_float = workbook.add_format({'num_format': '#,##0.0'})
        format_perc = workbook.add_format({'num_format': '0.0%'})
        header_format = workbook.add_format({
            'bold': True,
            'text_wrap': True,
            'valign': 'top',
            'border': 1})
        red_format = workbook.add_format({'bg_color': '#FFC7CE','font_color': '#9C0006'})
        green_format = workbook.add_format({'bg_color': '#C6EFCE','font_color': '#006100'})
        merge_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter'})
        worksheet = workbook.add_worksheet()

        groups = {
            'Totaux': {'format': format_number, 'base': 'total_', 'values': ['total_{}'.format(var) for var in variables.keys() if var != 'taux_positivite']},
            'Augmentations': {'format': format_number, 'base': 'aug_', 'values': ['aug_{}'.format(var) for var in variables.keys() if var != 'taux_positivite']},
            'Moyennes sur 7 jours': {'format': format_float, 'base': '_moy_7J', 'values': ['{}_moy_7J'.format(var) for var in variables.keys()] + ['perc_diff_{}_moy_7J'.format(var) for var in variables.keys()]},
            'Moyennes sur 14 jours': {'format': format_float, 'base': '_moy_14J', 'values': ['{}_moy_14J'.format(var) for var in variables.keys()] + ['perc_diff_{}_moy_14J'.format(var) for var in variables.keys()]}
        }

        columns = ['date'] + [item for elem in [x['values'] for x in groups.values()] for item in elem]
        df_export = df[columns]
        df_export = df_export.reindex(index=df_export.index[::-1])
        df_export['date'] = df_export['date'].dt.strftime('%Y-%m-%d')

        worksheet.merge_range('A1:A2', 'Date', merge_format)
        worksheet.write_column('A3:A{}'.format(len(df_export)+2), list(df_export[columns[0]]), None)

        worksheet.set_column(0, len(columns)-1, 20)

        startcol = 1
        for group, values in groups.items():
            endcol = startcol + len(values['values']) -1
            worksheet.merge_range('{}1:{}1'.format(letters[startcol], letters[endcol]), group, merge_format)
            startcol = endcol + 1
            
            for col in values['values']:
                colletter = letters[columns.index(col)]
                
                is_perc = 'perc' in col
                
                worksheet.write('{}2'.format(colletter), '{}{}'.format('% ' if is_perc else '', variables[col.replace(values['base'], '').replace('perc_diff_','')]))
                
                worksheet.write_column('{}3:{}{}'.format(colletter, colletter, len(df_export)+2), list(df_export[col]), format_perc if is_perc or 'taux_positivite' in col else values['format'])
                
                if 'perc' in col:
                    worksheet.conditional_format('{}3:{}{}'.format(colletter, colletter, len(df_export)+2), {'type':'cell',
                                                    'criteria': '>=',
                                                    'value':    0,
                                                    'format':   red_format if not 'test' in col and not 'vacc' in col else green_format})

                    worksheet.conditional_format('{}3:{}{}'.format(colletter, colletter, len(df_export)+2), {'type':'cell',
                                                    'criteria': '<',
                                                    'value':    0,
                                                    'format':   green_format if not 'test' in col and not 'vacc' in col else red_format})

        worksheet.set_row(1, None, header_format)
        worksheet.freeze_panes(2, 1)
        writer.save()        

        # Upload file to Nextcloud RTBF

        folders = today.strftime('%Y,%m,%d').split(',')
        base = event['data']['webdavpath']
        for folder in folders:
            base = '{}/{}'.format(base, folder)
            if not webdav.check(base):
                webdav.mkdir(base)                

        response = s3_resource.Object(bucket, file_name).put(Body=csv_buffer_internal.getvalue())

        response_presign = s3.generate_presigned_url('get_object', Params={'Bucket': bucket, 'Key': file_name}, ExpiresIn=event['data']['expiration_time'])
            
        # webdav.upload_sync(remote_path='{}/{}'.format(base, file_name), local_path=response_presign)

        # Send email

        subject = 'Calculette Sciensano : Mise à jour {}'.format(datetime.datetime.now().strftime('%d/%m/%Y'))

        body_html = """<html><head></head><body><h1>{}</h1><p>Un nouveau fichier a été généré. Pour le télécharger <a href="{}">cliquez ici</a>.</p></body></html>\nGestion des données, RTBF.""".format(subject, response_presign)
        # body_html = """<html><head></head><body><h1>{}</h1><p>Un nouveau fichier a été généré. Pour accéder rendez-vous sur <a href="{}">NextCloudRTBF</a>.</p></body></html> dans le dossier {}.""".format(subject, folder_share_link, '/'.join(folders))

        body_text = ('{}\r\nUn nouveau fichier a été généré. Pour le télécharger clickez sur le lien\n{}\nGestion des données, RTBF.'.format(subject, response_presign))
        # body_text = ('{}\r\nUn nouveau fichier a été généré. Pour accéder rendez-vous sur \n{}\ndans le dossier {}.'.format(subject, folder_share_link, '/'.join(folders)))

        response = ses.send_email(
            Destination={
                'ToAddresses': event['data']['recipients'],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': body_html,
                    },
                    'Text': {
                        'Charset': CHARSET,
                        'Data': body_text,
                    },
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': subject,
                },
            },
            Source=event['data']['sender']
        )

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Finished generating file {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), event['data']))
        }    

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error generating file {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), '{}, {}, {}'.format(exc_type, fname, exc_tb.tb_lineno)))
        }    