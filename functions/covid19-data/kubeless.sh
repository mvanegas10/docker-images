kubeless -n journalisme function delete sciensano-covid19
kubeless -n journalisme function deploy sciensano-covid19 --runtime python3.8 \
                                --from-file function.py \
                                --env SECRETS_FILE=aws-meiv \
                                --secrets aws-meiv \
                                --dependencies requirements.txt \
                                --timeout 3600 \
                                --handler function.handler

kubeless -n journalisme trigger cronjob create \
  sciensano-covid19-daily \
  --function sciensano-covid19 \
  --payload-from-file payload_daily.json \
  --schedule "0 4 * * *"