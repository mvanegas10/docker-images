kubeless function delete rfv-auvio-segments
kubeless function deploy rfv-auvio-segments --runtime python3.8 \
                                --from-file function.py \
                                --dependencies requirements.txt \
                                --env db_database=a360 \
                                --env db_host=10.200.0.84 \
                                --env db_port=5439 \
                                --env db_username=meiv \
                                --secrets aws-meiv \
                                --timeout 3600 \
                                --handler function.handler

kubeless trigger cronjob create \
  rfv-auvio-segments-cronjob \
  --function rfv-auvio-segments \
  --payload-from-file payload_quarter.json \
  --schedule "0 8 * * *"