# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import sqlalchemy as sql
import datetime
from sklearn.cluster import KMeans
from sklearn.preprocessing import QuantileTransformer
import unidecode
from botocore.config import Config
import botocore
import dateutil
import boto3
import json
import os
import io
import sys


# ## Utilitaires

def get_quarter(date):
    return (date.month - 1) // 3 + 1


def get_last_day_of_the_quarter(date):
    quarter = get_quarter(date)
    return datetime.datetime(date.year + 3*quarter//12, 3*quarter%12+1, 1) + datetime.timedelta(days=-1)


def get_first_day_of_the_quarter(date):
    return datetime.datetime(date.year, 3 * ((date.month - 1) // 3) + 1, 1)


def order_cluster(cluster_field_name, target_field_name, df, ascending):
    new_cluster_field_name = 'new_'+cluster_field_name
    df_new = df.groupby(cluster_field_name)[target_field_name].mean().reset_index()
    df_new = df_new.sort_values(by=target_field_name, ascending = ascending).reset_index(drop=True)
    df_new['index']=df_new.index
    df_final = pd.merge(df, df_new[[cluster_field_name, 'index']], on=cluster_field_name)
    df_final = df_final.drop([cluster_field_name], axis=1)
    df_final = df_final.rename(columns={'index':cluster_field_name})
    return df_final


def get_birth_deceny(age, years_list):
    for deceny in years_list:
        if age < deceny:
            return deceny


def handler(event, context):
    try:
        # Internal AWS resources
        aws_keys = {
            'AWS_ACCESS_KEY_ID': '',
            'AWS_SECRET_ACCESS_KEY': '',
            'AWS_DEFAULT_REGION': ''
        }

        for key in aws_keys:
            with open('/aws-meiv/{}'.format(key), 'r') as s3_f:
                aws_keys[key] = s3_f.read()        

        aws_config = Config(region_name=aws_keys['AWS_DEFAULT_REGION'])    

        aws_session = boto3.session.Session(
            aws_access_key_id=aws_keys['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=aws_keys['AWS_SECRET_ACCESS_KEY']            
        )

        aws_config = Config(region_name='eu-west-1')  

        ssm = aws_session.client('ssm', config=aws_config)
        s3 = aws_session.client('s3', config=aws_config)
        s3_resource = aws_session.resource('s3')  

        parameter = ssm.get_parameter(Name='REDSHIFT_MEIV_SECRET', WithDecryption=True)
        bucket = 'big-data-media'

        engine = sql.create_engine('postgresql://{}:{}@{}:{}/{}'.format(os.environ.get('db_username'), parameter['Parameter']['Value'], os.environ.get('db_host'), os.environ.get('db_port'), os.environ.get('db_database')),pool_recycle=600)

        years_list = event['data']['years_list']

        # ## Variables and constants
        limit_frequency = event['data']['limit_frequency']
        percentages = event['data']['percentages']
        
        ## Users frequency accepted quantile
        accepted_quantile = event['data']['accepted_quantile']

        today = datetime.datetime.now()
        start_date = get_first_day_of_the_quarter(today) if event['data']['type'] == 'current_quarter' else datetime.datetime(2020,1,1)
        end_date = get_last_day_of_the_quarter(today) + datetime.timedelta(days=1)  

        last = start_date
        quarters_list = [last]
        q = 0
        while last <= end_date:
            nq = get_quarter(last)
            if q != nq:
                q = nq
                quarters_list.append(last)
            last = last + dateutil.relativedelta.relativedelta(months=+1)

        quarters = {}

        for i, val in enumerate(quarters_list):
            if i+1 < len(quarters_list):
                quarters['Q{}{}'.format(get_quarter(val),val.year)] = {
                    'start': val.date(),
                    'strstart': val.date().strftime('%Y-%m-%d'),
                    'end': quarters_list[i+1].date() if quarters_list[i+1] < today else today.date(),
                    'strend': quarters_list[i+1].date().strftime('%Y-%m-%d') if quarters_list[i+1] < today else today.date().strftime('%Y-%m-%d')
                }


        rf_segment = event['data']['segments']

        segments = {}
        for key, value in rf_segment.items():
            if not value in segments:
                segments[value] = {'name': unidecode.unidecode(value), 'values':[]}
            segments[value]['values'].append(key)

        segments['fidèles']['color'] = '#E6ACAC'
        segments['occasionnels']['color'] = '#D4C4C4'
        segments['reconnus']['color'] = '#28D1D1'


        # ## Données
        
        df_users = pd.read_sql_query("""SELECT g.uid, LOWER(assignedgender) AS gender, date_part('year', CURRENT_DATE) - date_part('year', TO_DATE(birthdate, 'DD/MM/YYYY')) AS birth_deceny FROM egos_actito a INNER JOIN egos_gigya g ON a.actitoid = g.actitoid""", engine)
        df_users['birth_deceny'] = df_users['birth_deceny'].apply(lambda x: get_birth_deceny(x, years_list))
        df_users = df_users[df_users['birth_deceny'] <= 85]


        for quarter in quarters.keys():

            # ### Users Recence
            df_recency = pd.read_sql_query("""SELECT uid, MAX(date) as date FROM egos_conso_audio_video_connected_user WHERE media_type = context_media_type AND date >= '{}' AND date < '{}' GROUP BY uid""".format(quarters[quarter]['strstart'], quarters[quarter]['strend']), engine, parse_dates=['date'])
            df_recency = df_recency[~df_recency['date'].isna()]
            df_recency['recency'] = df_recency['date'].apply(lambda x: (quarters[quarter]['end'] - x.date()).days)


            # ### Users Frequency
            
            df_frequency = pd.read_sql_query("""SELECT uid, COUNT(date) AS frequency FROM egos_conso_audio_video_connected_user WHERE media_type = context_media_type AND date >= '{}' AND date < '{}' GROUP BY uid""".format(quarters[quarter]['strstart'], quarters[quarter]['strend']), engine)

            # ### Users Days
            
            df_days = pd.read_sql_query("""SELECT uid, COUNT(DISTINCT DATE_PART('day', date)) AS days FROM egos_conso_audio_video_connected_user WHERE media_type = context_media_type AND date >= '{}' AND date < '{}' GROUP BY uid""".format(quarters[quarter]['strstart'], quarters[quarter]['strend']), engine)

            df = df_recency.merge(df_frequency, on='uid', how='inner')
            df = df.merge(df_days, on='uid', how='left')

            df = df[~df['recency'].isna()]
            df = df[~df['frequency'].isna()]

            csv_buffer = io.StringIO()
            df.to_csv(csv_buffer, index=False, encoding='utf-8-sig')
            response = s3_resource.Object(bucket, 'crm/rfv_auvio_segments/{}/{}/{}_users_rfv.csv'.format(quarter[2:], quarter[:2], quarter)).put(Body=csv_buffer.getvalue())
            df_clustering = df.loc[df['frequency']<=df['frequency'].quantile(accepted_quantile)].copy()


            # ### Media consumption
            df_media_consummed = pd.read_sql_query("""SELECT * FROM ( WITH media_conso AS ( SELECT uid, contenttypestat, labelstat, COUNT(date) AS consummed FROM egos_conso_audio_video_connected_user conso INNER JOIN (SELECT * FROM ((SELECT mo.id, labelstat, contenttypestat FROM media_objects mo INNER JOIN media_programs mp ON mo.program_id = mp.id WHERE contenttypestat <> 'default') UNION ALL (SELECT lp.id, labelstat, contenttypestat FROM livecenter_planning lp INNER JOIN livecenter_live ll ON lp.live_id = ll.id INNER JOIN media_programs mp ON ll.program_id = mp.id WHERE contenttypestat <> 'default'))) media ON conso.media_id = media.id WHERE date >= '{}' AND date < '{}' GROUP BY uid, contenttypestat, labelstat ) SELECT uid, contenttypestat, labelstat, consummed, row_number() OVER(PARTITION BY uid ORDER BY consummed DESC) AS rnum_cnt FROM media_conso ) WHERE rnum_cnt<=2 AND consummed > 2""".format(quarters[quarter]['strstart'], quarters[quarter]['strend']), engine)
            csv_buffer = io.StringIO()
            df_media_consummed.to_csv(csv_buffer, index=False, encoding='utf-8-sig')
            response = s3_resource.Object(bucket, 'crm/rfv_auvio_segments/{}/{}/{}_user_most_labelstat_consumption.csv'.format(quarter[2:], quarter[:2], quarter)).put(Body=csv_buffer.getvalue())


            # ## Get recency and frequency groups
            kmeans = KMeans(n_clusters=3)
            kmeans.fit(df_clustering[['recency']])
            df_clustering['recencyCluster'] = kmeans.predict(df_clustering[['recency']])
            df_clustering = order_cluster('recencyCluster','recency', df_clustering, False)
            
            
            kmeans = KMeans(n_clusters=3)
            kmeans.fit(df_clustering[['frequency']])
            df_clustering['frequencyCluster'] = kmeans.predict(df_clustering[['frequency']])
            df_clustering = order_cluster('frequencyCluster','frequency', df_clustering, False)
            
            ey = np.array(df_clustering['frequency'])
            qt = QuantileTransformer(output_distribution='normal')
            df_clustering['procFrequency'] = qt.fit_transform(ey.reshape(-1, 1))
            kmeans = KMeans(n_clusters=3)
            kmeans.fit(df_clustering[['procFrequency']])
            df_clustering['frequencyCluster'] = kmeans.predict(df_clustering[['procFrequency']])
            df_clustering = order_cluster('frequencyCluster','frequency', df_clustering, False)

            
            df_clustering['rf'] = df_clustering['recencyCluster'].astype(str) + df_clustering['frequencyCluster'].astype(str)
            
            df_clustering['segment'] = df_clustering['rf'].apply(lambda x: rf_segment[x])
            
            df = df.merge(df_clustering[['uid','recencyCluster','frequencyCluster','rf','segment']], how='left',on='uid')
            
            
            df['segment'] = df['segment'].apply(lambda x: x if x == x else 'outliers')
            
            df_clustering = df_clustering.merge(df_users, on='uid',how='left')


            # ## Export
            
            df_summary = df[['recency','frequency', 'segment']]
            df_update = df_summary.loc[df_summary['segment'] == 'outliers'].copy()
            avg_frequency = df_update['frequency'].mean()
            avg_recency = df_update['recency'].mean()
            df_update['frequency'] = limit_frequency if avg_frequency > limit_frequency else avg_frequency
            df_update['recency'] = avg_recency 
            df_summary.update(df_update)
            df_summary['recency'] = df_summary['recency'].apply(lambda x: 3 * round(x/3))
            df_summary['frequency'] = df_summary['frequency'].apply(lambda x: 60 * round(x/60))
            df_summary = df_summary.groupby(['recency','frequency','segment']).size().reset_index().rename(columns={0:'utilisateurs'})


            # ### Metadata
            
            df_clustering.groupby(['segment','gender','birth_deceny']).size().reset_index().rename(columns={0:'users'}).to_dict(orient='records')
            
            summary_data = bytearray('{{ {}, "summary": {}, "sociodemo" : {} }}'.format(','.join(['"{}": {}'.format(name,json.dumps(df[df['rf'].isin(seg['values'])][['frequency','recency','days']].describe().to_dict(orient='dict'))) for name, seg in segments.items() if 'values' in seg]), json.dumps(df_clustering.groupby(['recencyCluster','frequencyCluster','segment']).size().reset_index().rename(columns={0:'users'}).to_dict(orient='records')), json.dumps(df_clustering.groupby(['segment','gender','birth_deceny']).size().reset_index().rename(columns={0:'users'}).to_dict(orient='records'))), 'utf8')
            df_consumption_global = df_media_consummed.merge(df_clustering, on='uid', how='inner')
        
            summary_private_file = 'crm/rfv_auvio_segments/{}/{}/{}_rf_segments_summary.json'.format(quarter[2:], quarter[:2], quarter)
            response = s3_resource.Object(bucket, summary_private_file).put(Body=summary_data)

            for segment in [x for x in segments.values() if 'values' in x]:
                for accepted_percentage_consumption in percentages:
                    df_consumption_segment = df_consumption_global[df_consumption_global['rf'].isin(segment['values'])]
                    df_consumption_segment.shape
                    
                    df_consumption_segment['labelstat'].unique().shape
                    
                    df_labelstats = df_consumption_segment.copy()
                    df_labelstats = df_labelstats[['labelstat','contenttypestat']].groupby(['labelstat','contenttypestat']).size().reset_index().rename(columns={0:'consummed'}).sort_values('consummed', ascending=False)
                    
                    df_labelstats['percentage'] = df_labelstats['consummed']/np.sum(df_labelstats['consummed'])
                    
                    df_labelstats['cumsum'] = df_labelstats['percentage'].cumsum()
                    df_labelstats.head(50)
                    
                    df_labelstats = df_labelstats[df_labelstats['cumsum'] <= accepted_percentage_consumption]
                    
                    labelstat_subset = df_labelstats['labelstat']
                    labelstat_subset
                    
                    df_consumption_segment_media_perc = df_consumption_segment[df_consumption_segment['labelstat'].isin(labelstat_subset)]
                    
                    df_nodes = df_labelstats.copy()
                    df_nodes = df_nodes[['labelstat', 'contenttypestat', 'consummed']].rename(columns={'labelstat':'name', 'contenttypestat': 'grp', 'consummed':'n'})
                    df_nodes['id'] = df_nodes['name']
                    df_nodes['sociodemo'] = df_nodes['name'].apply(lambda x: df_consumption_segment[df_consumption_segment['labelstat'] == x].groupby(['gender','birth_deceny']).size().reset_index().rename(columns={0:'users'}).to_dict(orient='records'))


                    # ### Nodes
                    
                    df_consumption_segment_media_perc = df_consumption_segment_media_perc.sort_values(['uid','consummed'], ascending=False)
                    
                    df_links = df_consumption_segment_media_perc.groupby('uid')['labelstat'].apply(lambda x: '{{ "source":"{}", "target":"{}" }}'.format(list(x)[0], list(x)[1] if len(list(x)) > 1 else '')).reset_index()
                    
                    df_links['source'] = df_links['labelstat'].apply(lambda x : json.loads(x)['source'])
                    df_links['target'] = df_links['labelstat'].apply(lambda x : json.loads(x)['target'])
                    
                    df_links = df_links[['source','target','uid']].groupby(['source','target']).count().reset_index().rename(columns={'uid':'value'})
                    
                    df_links = df_links[df_links['target'] != '']
                    
                    df_links = df_links[df_links['source'] != df_links['target']]


                    network_data = bytearray('{{ "nodes":{}, "links": {}, "attributes": "" }}'.format(json.dumps(df_nodes.to_dict(orient='records')),json.dumps(df_links.to_dict(orient='records'))), 'utf8')
                    
                    network_private_file = 'crm/rfv_auvio_segments/{}/{}/{}_conso_network_{}_{}.json'.format(quarter[2:], quarter[:2], quarter, int(accepted_percentage_consumption*100), segment['name'])
                    response = s3_resource.Object(bucket, network_private_file).put(Body=network_data)

            obj = {}
            
            summary = 'crm/rfv_auvio_segments/{}/{}/{}_rf_segments_summary.json'.format(quarter[2:], quarter[:2], quarter)
            
            data = {}
            for p in percentages:
                perc = int(p*100)
                for seg in [x['name'] for x in segments.values()]:
                    filename = 'crm/rfv_auvio_segments/{}/{}/{}_conso_network_{}_{}.json'.format(quarter[2:], quarter[:2], quarter, perc, seg)
                    try:
                        s3_resource.Object(bucket, filename).load()
                        filedata = json.load(io.BytesIO(s3.get_object(Bucket=bucket, Key=filename)['Body'].read()))

                        if not quarter.lower() in data:
                            data[quarter.lower()] = {}
                        if not perc in data[quarter.lower()]:
                            data[quarter.lower()][perc] = {}
                        if not seg in data[quarter.lower()][perc]:
                            data[quarter.lower()][perc][seg] = {} 
                        
                        data[quarter.lower()][perc][seg] = filedata                     
                    except botocore.exceptions.ClientError as e:
                        print(f'Error: file {filename} not found')
                        
            try:
                s3_resource.Object(bucket, summary).load()
                if quarter.lower() in data:
                    filedata = json.load(io.BytesIO(s3.get_object(Bucket=bucket, Key=summary)['Body'].read()))
                    obj = {
                        'summary': {
                            quarter.lower() : filedata,
                        },
                        'network': data
                    }
                    buffer_output = io.StringIO()
                    json.dump(obj, buffer_output)
                    response = s3_resource.Object(bucket, 'crm/rfv_auvio_segments/{}/{}/{}_entire_data.json'.format(quarter[2:], quarter[:2], quarter)).put(Body=buffer_output.getvalue())
                    print(response)  
            except botocore.exceptions.ClientError as e:
                print(f'Error: file {summary} not found')  

        return {
            'statusCode': 200,
            'body': json.dumps('{} : Finished files generation.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S')))
        }

    except Exception as e:
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

        return {
            'statusCode': 500,
            'body': json.dumps('{} : Error generating files {} at {}.'.format(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'), e, exc_type, exc_obj, exc_tb.tb_lineno))
        }